<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Laporan Peminjaman Harian : <?php echo $tes?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form method="post" action="<?php echo base_url(). 'laporan/peminjam_harian'; ?>">
                    <label>Tanggal : </label>
                     <input type="date" name="tanggal" required="">
                    <input type="submit" value="OK" class="btn bg-purple  btn-xs">
              </form>
              <form method="post" action="<?php echo base_url(). 'cetak/peminjam_harian'; ?>">
                    <input type="hidden" name="tanggal" value="<?php echo $tes?>">
                    <button type="submit" class="btn bg-purple pull-right btn-sm"><i class="fa fa-print"></i> cetak</button>
              </form>
              <br><br>
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>Nama</th>
                  <th>Judul Buku</th>
                  <th>Jumlah</th>
                  <th>Tanggal Pinjam</th>
                  <th>tanggal Kembali</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                $n=1;
                $asr=date('Y-m-d');
                foreach($peminjam as $p){ 
                ?>
                <tr>
                  <td><?php echo $n++ ?></td>
                  <td><?php echo $p->nama_siswa?></td>
                  <td><?php echo $p->judul?></td>
                  <td><?php echo $p->jml?></td>
                  <td><?php echo $p->tgl_pinjam?></td>
                  <td><?php echo $p->tgl_kembali?></td>
                </tr>
              <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>NO</th>
                  <th>Nama</th>
                  <th>Judul Buku</th>
                  <th>Jumlah</th>
                  <th>Tanggal Pinjam</th>
                  <th>tanggal Kembali</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    