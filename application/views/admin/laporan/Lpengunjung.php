
          <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
              <li><a href="#tguru" data-toggle="tab"><button class="btn btn-info">Guru</button></a></li>
              <li><a href="#tsiswa" data-toggle="tab"><button class="btn btn-primary">Siswa</button></a></li>
              <li class="pull-left header">
              <div class="box-title">
                <h3><i class="fa fa-users"></i>Laporan Pengunjung</i></h3>
              </div>
            </li>
               <form method="post" action="<?php echo base_url(). 'laporan/Lpengunjung'; ?>"><br>
                    <div class="col-sm-1"></div>
                    <label>Tanggal : </label>
                     <input type="date" name="tanggal" required="">
                    <label>s/d</label>
                     <input type="date" name="tanggal2" required="">
                    <input type="submit" value="OK" class="btn bg-purple  btn-xs">
              </form>
            </ul>
            <!-- Tabs within a box -->
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div class="chart tab-pane active" id="tsiswa">
              <div class="box-body">
              <div class="box">
            <div class="box-header">
              <h3 class="pull-left box-title">Siswa  <br><br><?php echo $tes ?> s/d <?php echo $tes2 ?></h3> 
              <form method="post" action="<?php echo base_url(). 'laporanpdf'; ?>">
                    <input type="hidden" name="tanggal" value="<?php echo $tes ?>">
                    <input type="hidden" name="tanggal2" value="<?php echo $tes2 ?>">
                    <button type="submit" class="btn bg-purple pull-right btn-sm"><i class="fa fa-print"></i> cetak</button>
              </form>
           </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table width="100%" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Waktu</th>
                  <th>Nama</th>
                  <th>Kelas</th>
                  <th>Keterangan</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $n=1;
                foreach($pengunjung as $p){ 
                ?>
                <tr>
                  <td><?php echo $n++?></td>
                  <td><?php echo $p->tanggal?></td>
                  <td><?php echo $p->nama_siswa?></td>
                  <td><?php echo $p->id_kejur?></td>
                  <td><?php echo $p->keterangan?></td>
                  <td>
                    <center><button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-info"  onclick="tampildata('<?php echo $p->id_pengunjung ?>','<?php echo $p->nama_siswa ?>','<?php echo $p->id_kejur ?>','<?php echo $p->keterangan ?>')"><i class="fa fa-edit"></i> edit</button> 
                  <?php echo anchor('laporan/hapusls/'.$p->id_pengunjung,'<button class="btn btn-danger"><i class="fa fa-trash"></i>Hapus</button>'); ?></center>
                </td>
                </tr>
              <?php }?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Waktu</th>
                  <th>Nama</th>
                  <th>Kelas</th>
                  <th>Keterangan</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
            </div>
              </div>
              <div class="chart tab-pane" id="tguru">
                <div class="box-body">
              <div class="box">
            <div class="box-header">
              <h3 class="pull-left box-title">Guru  <br><br><?php echo $tes ?> s/d <?php echo $tes2 ?></h3> 
              <form method="post" action="<?php echo base_url(). 'laporanpdf'; ?>">
                    <input type="hidden" name="tanggal" value="<?php echo $tes ?>">
                    <input type="hidden" name="tanggal2" value="<?php echo $tes2 ?>">
                    <button type="submit" class="btn bg-purple pull-right btn-sm"><i class="fa fa-print"></i> cetak</button>
              </form>
           </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table width="100%" id="example3" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Waktu</th>
                  <th>Nama</th>
                  <th>Jabatan</th>
                  <th>Keterangan</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $n=1;
                foreach($gpengunjung as $g){ 
                ?>
                <tr>
                  <td><?php echo $n++?></td>
                  <td><?php echo $g->tanggal?></td>
                  <td><?php echo $g->nama?></td>
                  <td><?php echo $g->jabatan?></td>
                  <td><?php echo $g->keterangan?></td>
                  <td>
                    <center><button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-info2"  onclick="tampildata2('<?php echo $g->id_gpengunjung ?>','<?php echo $g->nama ?>','<?php echo $g->jabatan ?>','<?php echo $g->keterangan ?>')"><i class="fa fa-edit"></i> edit</button> 
                  <?php echo anchor('laporan/hapuslg/'.$g->id_gpengunjung,'<button class="btn btn-danger"><i class="fa fa-trash"></i>Hapus</button>'); ?></center>
                </td>
                </tr>
              <?php }?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Waktu</th>
                  <th>Nama</th>
                  <th>Kelas</th>
                  <th>Keterangan</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
            </div>
              </div>
            </div>
          </div>



          <div class="modal modal-default fade" id="modal-info" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Keterangan Pengunjung</h4>
              </div>
              <div class="modal-body box-header">
              
<div class="box box-info">
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url(). 'laporan/updatels'; ?>" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-3">Nama Siswa</label>

                  <div class="col-sm-8">
                <input type="hidden" class="form-control" name="id_pengunjung" id="s">
                    <input type="text" readonly="" class="form-control" name="nama_siswa" id="nama">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Kelas/Jurusan</label>

                  <div class="col-sm-8">
                    <input type="text" readonly="" class="form-control" name="id_kejur" id="kejur">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Keterangan</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="keterangan" id="ket">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          
              </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </div>


      <div class="modal modal-default fade" id="modal-info2" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Keterangan Pengunjung</h4>
              </div>
              <div class="modal-body box-header">
              
<div class="box box-info">
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url(). 'laporan/updatelg'; ?>" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-3">Nama Guru</label>

                  <div class="col-sm-8">
                <input type="hidden" class="form-control" name="id_gpengunjung" id="g">
                    <input type="text" readonly="" class="form-control" name="nama" id="namag">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Jabatan</label>

                  <div class="col-sm-8">
                    <input type="text" readonly="" class="form-control" name="jabatan" id="jabatan">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Keterangan</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="keterangan" id="ketg">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          
              </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </div>
        <!-- /.modal -->
 <script>
    function tampildata( id_pengunjung, nama_siswa, id_kejur, keterangan){
      $('#nama').val(nama_siswa);
      $('#kejur').val(id_kejur);
      $('#ket').val(keterangan);
      $('#s').val(id_pengunjung);
      
    }
    function tampildata2( id_gpengunjung, nama, jabatan, keterangan){
      $('#namag').val(nama);
      $('#jabatan').val(jabatan);
      $('#ketg').val(keterangan);
      $('#g').val(id_gpengunjung);
      
    }
  </script>
        