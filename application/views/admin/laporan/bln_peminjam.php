<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Laporan Peminjam Bulanan : <?php
              if ($lbl1=="01") {
                echo "Januari";
              }elseif ($lbl1=="02") {
                echo "Februari";
              }elseif ($lbl1=="03") {
                echo "Maret";
              }elseif ($lbl1=="04") {
                echo "April";
              }elseif ($lbl1=="05") {
                echo "Mei";
              }elseif ($lbl1=="06") {
                echo "Juni";
              }elseif ($lbl1=="07") {
                echo "Juli";
              }elseif ($lbl1=="08") {
                echo "Agustus";
              }elseif ($lbl1=="09") {
                echo "September";
              }elseif ($lbl1=="10") {
                echo "Oktober";
              }elseif ($lbl1=="11") {
                echo "November";
              }elseif ($lbl1=="12") {
                echo "Desember";
              }
               ?>-<?php echo $lbl2 ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form method="post" action="<?php echo base_url(). 'laporan/Peminjam_bulanan'; ?>">
                    <label>Bulan : </label>
                      <input type='text'
                                   required="" 
                                   placeholder="--Bulan--"
                                   class='flexdatalist'
                                   data-min-length='1'
                                   data-selection-required='true'
                                   list='languages'
                                   data-search-by-word='true'
                                   name='bulan'>

                            <datalist id='languages'>
                                <option> Januari(01) </option>
                                <option> Februari(02) </option>
                                <option> Maret(03) </option>
                                <option> April(04) </option>
                                <option> Mei(05) </option>
                                <option> Juni(06) </option>
                                <option> Juli(07) </option>
                                <option> Agustus(08) </option>
                                <option> September(09) </option>
                                <option> oktober(10) </option>
                                <option> November(11) </option>
                                <option> Desember(12) </option>
                            </datalist>
                            <label>Thn : </label>
                      <input type="number" name="tahun" min="2019" required="">
                    <input type="submit" value="OK" class="btn bg-purple  btn-sm">
              </form>
              <form method="post" action="<?php echo base_url(). 'cetak/peminjam_bulanan'; ?>">
                      <input type="hidden" name="bulan" value="<?php echo $lbl1 ?>">
                      <input type="hidden" name="tahun" value="<?php echo $lbl2 ?>">
                     <button type="submit" class="btn bg-purple pull-right btn-xs"><i class="fa fa-print"></i> cetak</button>
                    <select name="jurusan" class="pull-right">
                      <option value="AKL" selected="selected"> AKL </option>
                      <option value="BDP" > BDP </option>
                      <option value="MM" > MM </option>
                      <option value="OTKP" > OTKP </option>
                      <option value="RPL" > RPL </option>
                    </select>
              </form>
              <br><br>
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>Tanggal</th>
                  <th>REF</th>
                  <th>000</th>
                  <th>100</th>
                  <th>200</th>
                  <th>300</th>
                  <th>400</th>
                  <th>500</th>
                  <th>600</th>
                  <th>700</th>
                  <th>800</th>
                  <th>900</th>
                  <th>Jumlah</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-01</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <!-- <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-02</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-03</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-04</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-05</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-06</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-07</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-08</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-09</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-10</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-11</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-12</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-13</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-14</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-15</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-16</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-17</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-18</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-19</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-20</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-21</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-22</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-23</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-24</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-25</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-26</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-27</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-28</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-29</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-30</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th><?php echo $lbl2 ?>-<?php echo $lbl1 ?>-31</th>
                  <td><?php echo $peminjam001 ?></td>
                  <td><?php echo $peminjam101 ?></td>
                  <td><?php echo $peminjam201 ?></td>
                  <td><?php echo $peminjam301 ?></td>
                  <td><?php echo $peminjam401 ?></td>
                  <td><?php echo $peminjam501 ?></td>
                  <td><?php echo $peminjam601 ?></td>
                  <td><?php echo $peminjam701 ?></td>
                  <td><?php echo $peminjam801 ?></td>
                  <td><?php echo $peminjam901 ?></td>
                  <td><?php echo $peminjam1001 ?></td>
                  <td></td>
                </tr>
                 --></tbody>
                <tfoot>
                <tr>
                  <th>Tanggal</th>
                  <th>REF</th>
                  <th>000</th>
                  <th>100</th>
                  <th>200</th>
                  <th>300</th>
                  <th>400</th>
                  <th>500</th>
                  <th>600</th>
                  <th>700</th>
                  <th>800</th>
                  <th>900</th>
                  <th>Jumlah</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    