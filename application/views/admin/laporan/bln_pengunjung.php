<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Laporan Pengunjung Bulanan : <?php
              if ($lbl1=="01") {
                echo "Januari";
              }elseif ($lbl1=="02") {
                echo "Februari";
              }elseif ($lbl1=="03") {
                echo "Maret";
              }elseif ($lbl1=="04") {
                echo "April";
              }elseif ($lbl1=="05") {
                echo "Mei";
              }elseif ($lbl1=="06") {
                echo "Juni";
              }elseif ($lbl1=="07") {
                echo "Juli";
              }elseif ($lbl1=="08") {
                echo "Agustus";
              }elseif ($lbl1=="09") {
                echo "September";
              }elseif ($lbl1=="10") {
                echo "Oktober";
              }elseif ($lbl1=="11") {
                echo "November";
              }elseif ($lbl1=="12") {
                echo "Desember";
              }
               ?>-<?php echo $lbl2 ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form method="post" action="<?php echo base_url(). 'laporan/pengunjung_bulanan'; ?>">
                    <label>Bulan : </label>
                      <input type='text'
                                   required="" 
                                   placeholder="--Bulan--"
                                   class='flexdatalist'
                                   data-min-length='1'
                                   data-selection-required='true'
                                   list='languages'
                                   data-search-by-word='true'
                                   name='bulan'>

                            <datalist id='languages'>
                                <option> Januari(01) </option>
                                <option> Februari(02) </option>
                                <option> Maret(03) </option>
                                <option> April(04) </option>
                                <option> Mei(05) </option>
                                <option> Juni(06) </option>
                                <option> Juli(07) </option>
                                <option> Agustus(08) </option>
                                <option> September(09) </option>
                                <option> oktober(10) </option>
                                <option> November(11) </option>
                                <option> Desember(12) </option>
                            </datalist>
                            <label>Thn : </label>
                      <input type="number" name="tahun" min="2019" required="">
                    <input type="submit" value="OK" class="btn bg-purple  btn-sm">
              </form>
              <form method="post" action="<?php echo base_url(). 'cetak/pengunjung_bulanan'; ?>">
                      <input type="hidden" name="bulan" value="<?php echo $lbl1 ?>">
                      <input type="hidden" name="tahun" value="<?php echo $lbl2 ?>">
                     <button type="submit" class="btn bg-purple pull-right btn-xs"><i class="fa fa-print"></i> cetak</button>
                    <select name="jurusan" class="pull-right">
                      <option value="AKL" selected="selected"> AKL </option>
                      <option value="BDP" > BDP </option>
                      <option value="MM" > MM </option>
                      <option value="OTKP" > OTKP </option>
                      <option value="RPL" > RPL </option>
                    </select>
              </form>
              <br><br>
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>Nama</th>
                  <th>Kelas</th>
                  <th>Kegiatan</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $n=1;
                foreach($pengunjung as $p){ 
                ?>
                <tr>
                  <td><?php echo $n++ ?></td>
                  <td><?php echo $p->nama_siswa?></td>
                  <td><?php echo $p->id_kejur?></td>
                  <td><?php echo $p->keterangan?></td>
                </tr>
                <?php }?>
                </tbody>
                <tfoot>
                <tr>
                  <th>NO</th>
                  <th>Nama</th>
                  <th>Kelas</th>
                  <th>Kegiatan</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    