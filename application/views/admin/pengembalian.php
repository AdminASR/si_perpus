
<div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
              <li><a href="#guru1" data-toggle="tab"><button class="btn btn-info">Guru</button></a></li>
              <li><a href="#siswa1" data-toggle="tab"><button class="btn btn-primary">Siswa</button></a></li>
              <li class="pull-left header"><i class="fa fa-book"></i>Tabel Pengembalian Buku</li>
            </ul>
            <!-- Tabs within a box -->
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div class="chart tab-pane active" id="siswa1">
      <div class="box-header">
              <h3 class="box-title"><b>Siswa</b></h3>
    </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>Nama Peminjam</th>
                  <th>Kelas</th>
                  <th>tgl Pinjam</th>
                  <th>tgl Kembali</th>
                  <th><center>aksi</center></th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $n=1;
                foreach($peminjam as $p){ 
                ?>
                <tr>
                  <td><?php echo $n++?></td>
                  <td ><?php echo $p->nama_siswa?></td>
                  <td><?php echo $p->id_kejur?></td>
                  <td><?php echo $p->tgl_pinjam?></td>
                  <td><?php echo $p->tgl_kembali?></td>
                  <td>
                    <center>
                      <?php echo anchor('peminjam/s_detail/'.$p->id_pinjam,'<button class="btn btn-primary"><i class="fa fa-file-text"></i> detail</button>');?>
                      <button class="btn btn-success"><i class="fa fa-pencil"></i> edit</button>
                    </center>
                </td>
                </tr>
              <?php }?>
                </tbody>
                <tfoot>
                <tr>
                  <th>NO</th>
                  <th>Nama Peminjam</th>
                  <th>Kelas</th>
                  <th>tgl Pinjam</th>
                  <th>tgl Kembali</th>
                  <th><center>aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>

              <div class="chart tab-pane" id="guru1">
              <div class="box-header">
              <h3 class="box-title"><b>Guru</b></h3>
    </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example3" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>Nama Peminjam</th>
                  <th>Jabatan</th>
                  <th>tgl Pinjam</th>
                  <th>tgl Kembali</th>
                  <th><center>aksi</center></th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $n=1;
                foreach($gpeminjam as $g){ 
                ?>
                <tr>
                  <td><?php echo $n++?></td>
                  <td ><?php echo $g->nama?></td>
                  <td><?php echo $g->jabatan?></td>
                  <td><?php echo $g->tgl_pinjam?></td>
                  <td><?php echo $g->tgl_kembali?></td>
                  <td>
                    <center><?php echo anchor('peminjam/g_detail/'.$g->id_gpinjam,'<button class="btn btn-primary"><i class="fa fa-file-text"></i> detail</button>');?>
                      <button class="btn btn-success"><i class="fa fa-pencil"></i> edit</button>
                    </center>
                </td>
                </tr>
              <?php }?>
                </tbody>
                <tfoot>
                <tr>
                  <th>NO</th>
                  <th>Nama Peminjam</th>
                  <th>Jabatan</th>
                  <th>tgl Pinjam</th>
                  <th>tgl Kembali</th>
                  <th><center>aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>

              </div>
            </div>
          </div>
<script type="text/javascript">
   function tambahHobi() {
     var idf = document.getElementById("idf").value;
     var stre;
     var asr=1;
     stre="<p id='srow" + idf + "'><br><input type='text' required='' placeholder='--judul buku--' class='flexdatalist form-control' data-min-length='1' data-selection-required='true' list='buku" + asr + "' name='kode_buku[]'><datalist id='buku" + asr + "'><?php 
                              foreach($buku as $p){ 
                              ?>
                                <option><?php echo $p->judul?> (<?php echo $p->kode_buku?>)</option><?php }?>
                            </datalist><input type='number' name='jml[]' min='1' size='20' placeholder='--jumlah--'><a href='#' style=\"color:#3399FD;\" onclick='hapusElemen(\"#srow" + idf + "\"); return false;'> <button class='btn btn-danger'><i class='fa fa-minus'></i></button></a>";
     $("#divHobi").append(stre);
     idf = (idf-1) + 2;
     document.getElementById("idf").value = idf;
   }
   function hapusElemen(idf) {
     $(idf).remove();
   }
   function tambahBuku() {
     var idfg = document.getElementById("idfg").value;
     var stre;
     var asr=1;
     stre="<p id='srow" + idfg + "'><br><input type='text' required='' placeholder='--judul buku--' class='flexdatalist form-control' data-min-length='1' data-selection-required='true' list='buku" + asr + "' name='kode_buku[]'><datalist id='buku" + asr + "'><?php 
                              foreach($buku as $p){ 
                              ?>
                                <option><?php echo $p->judul?> (<?php echo $p->kode_buku?>)</option><?php }?>
                            </datalist><input type='number' name='jml[]' min='1' size='20' placeholder='--jumlah--'><a href='#' style=\"color:#3399FD;\" onclick='hapusElemeng(\"#srow" + idfg + "\"); return false;'> <button class='btn btn-danger'><i class='fa fa-minus'></i></button></a>";
     $("#divBuku").append(stre);
     idfg = (idfg-1) + 2;
     document.getElementById("idfg").value = idfg;
   }
   function hapusElemeng(idfg) {
     $(idfg).remove();
   }
</script>