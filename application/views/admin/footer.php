
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Admin</b>ASR<b>&</b><b>Faisal</b>_ID
    </div>
    <strong>Copyright &copy; 2019 @RPL_SMKN_6_Jember.</strong> SI_perpustakaan
  </footer>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/dist/js/demo.js')?>"></script>
<script src="<?php echo base_url('assets/jquery-flexdatalist-2.2.4/jquery.flexdatalist.min.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example4').DataTable()
    $('#example3').DataTable()
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  });
  $('.flexdatalist').flexdatalist({
     selectionRequired: true,
     searchByWord: true,
     minLength: 1
      });
   $('.flexdatalist1').flexdatalist({
     selectionRequired: true,
     searchByWord: true,
     minLength: 1
      });

   function kofirmasi(){
        var konfirmasi = confirm("Silakan Klik Tombol Salah Satu Tombol");
        var text = "";
        
        if(konfirmasi == true) {
          text = "Kamu klik Tombol OK";
        }else{
          text = "Kamu klik Tombol Cancel";
        }
        
        document.getElementById("konfirm").innerHTML = text;
      }
      function terkirim(){
       alert("Data Berhasil Terkirim! ");
      }
      function hapus(){
       alert("Yakin hapus?. ");
      }
</script>
</body>
</html>
