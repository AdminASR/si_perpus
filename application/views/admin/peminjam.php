
<div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
              <li><a href="#guru" data-toggle="tab"><button class="btn btn-info">Guru</button></a></li>
              <li><a href="#siswa" data-toggle="tab"><button class="btn btn-primary">Siswa</button></a></li>
              <li class="pull-left header"><i class="fa fa-area-chart"></i>Form Peminjaman Buku</li>
            </ul>
            <!-- Tabs within a box -->
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div class="chart tab-pane active" id="siswa">
      <form role="form" method="post" action="<?php echo base_url('peminjam/tambah_peminjam')?>">
          <div class="col-xs-3">
              <div class="box-body">
              <h3 class="box-title">Siswa</h3>
                <div class="form-group">
                  <label for="exampleInputPassword1">Cari Nama Siswa</label><br>
                  <input type='text'
                                  required="" 
                                   placeholder="--nama siswa--"
                                   class='flexdatalist form-control'
                                   data-min-length='1'
                                   data-selection-required='true'
                                   list='siswa'
                                   data-search-by-word='true'
                                   name='nis'>

                            <datalist id='siswa'>
                              <?php 
                              foreach($siswa as $s){ 
                              ?>
                                <option><?php echo $s->nama_siswa?> [<?php echo $s->nis?>] <?php echo $s->id_kejur?></option>
                              <?php }?>
                            </datalist>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Keterangan</label>
                  <input type="text" class="form-control" name="keterangan" id="exampleInputPassword1" placeholder="keterangan">
                </div>
              </div>
          </div>
          <div class="col-xs-6">
              <div class="box-body">
              <h3 class="box-title">Buku</h3>
                <div class="form-group">
                  <label for="exampleInputPassword1">Judul</label><br>
                   <input id="idf" name='idf' value="1" type="hidden" >
                   <input name='status' value="1" type="hidden" >
                   <div class="row">
                   <input type='text'
                                  required="" 
                                   placeholder="--judul buku--"
                                   class='flexdatalist form-control'
                                   data-min-length='1'
                                   data-selection-required='true'
                                   list='buku'
                                   name='kode_buku[]'>

                            <datalist id="buku">
                              <?php 
                              foreach($buku as $b){ 
                              ?>
                                <option><?php echo $b->judul?> [<?php echo $b->kode_buku?>]</option>
                              <?php }?>
                            </datalist>

                   <input type="number" name="jml[]" min="1" placeholder=" --jumlah--">
                   <button class="btn btn-success " type="button" onclick="tambahHobi(); return false;"><i class="fa fa-plus"></i></button>
                   <div id="divHobi"></div>                                
                 </div>
               </div>
                              </div>
                          </div>
                          <div class="col-xs-3">
                              <div class="box-body">
                              <h3 class="box-title">Tanggal</h3>
                                 <div class="form-group">
                                  <label for="exampleInputPassword1">peminjaman</label>
                                  <input type="date" class="form-control" name="tgl_pinjam" value="<?php echo $date= date('Y-m-d');?>" >
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputPassword1">pengembalian</label>
                                  <input type="date" class="form-control" name="tgl_kembali"  >
                                </div>
                              </div>
                          </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                            </div>
                  </form>

              </div>
              <div class="chart tab-pane" id="guru">
    <form role="form" method="post" action="<?php echo base_url('peminjam/tambah_gpeminjam')?>">
          <div class="col-xs-3">
              <div class="box-body">
              <h3 class="box-title">Guru</h3>
                <div class="form-group">
                  <label for="exampleInputPassword1">Cari Nama Guru</label><br>
                  <input type='text'
                                  required="" 
                                   placeholder="--nama guru--"
                                   class='flexdatalist form-control'
                                   data-min-length='1'
                                   data-selection-required='true'
                                   list='gurU'
                                   data-search-by-word='true'
                                   name='no_anggota'>

                            <datalist id='gurU'>
                              <?php 
                              foreach($guru as $g){ 
                              ?>
                                <option><?php echo $g->nama?> [<?php echo $g->no_anggota?>]</option>
                              <?php }?>
                            </datalist>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Keterangan</label>
                  <input type="text" class="form-control" name="keterangan" id="exampleInputPassword1" placeholder="keterangan">
                </div>
              </div>
          </div>
          <div class="col-xs-6">
              <div class="box-body">
              <h3 class="box-title">Buku</h3>
                <div class="form-group">
                  <label for="exampleInputPassword1">Judul</label><br>
                   <input id="idfg" name='idfg' value="1" type="hidden" >
                   <input name='status' value="1" type="hidden" >
                   <div class="row">
                   <input type='text'
                                  required="" 
                                   placeholder="--judul buku--"
                                   class='flexdatalist form-control'
                                   data-min-length='1'
                                   data-selection-required='true'
                                   list='buku'
                                   name='kode_buku[]'>

                            <datalist id="buku">
                              <?php 
                              foreach($buku as $b){ 
                              ?>
                                <option><?php echo $b->judul?> [<?php echo $b->kode_buku?>]</option>
                              <?php }?>
                            </datalist>

                   <input type="number" name="jml[]" min="1" placeholder=" --jumlah--">
                   <button class="btn btn-success " type="button" onclick="tambahBuku(); return false;"><i class="fa fa-plus"></i></button>
                   <div id="divBuku"></div>                                
                 </div>
               </div>
              </div>
            </div>
                          <div class="col-xs-3">
                              <div class="box-body">
                              <h3 class="box-title">Tanggal</h3>
                                 <div class="form-group">
                                  <label for="exampleInputPassword1">peminjaman</label>
                                  <input type="date" class="form-control" name="tgl_pinjam" value="<?php echo $date= date('Y-m-d');?>" >
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputPassword1">pengembalian</label>
                                  <input type="date" class="form-control" name="tgl_kembali" >
                                </div>
                              </div>
                          </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                            </div>
                  </form>

              </div>
            </div>
          </div>

<div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
              <li><a href="#guru1" data-toggle="tab"><button class="btn btn-info">Guru</button></a></li>
              <li><a href="#siswa1" data-toggle="tab"><button class="btn btn-primary">Siswa</button></a></li>
              <li class="pull-left header"><i class="fa fa-book"></i>Tabel Peminjaman Buku</li>
            </ul>
            <!-- Tabs within a box -->
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div class="chart tab-pane active" id="siswa1">
      <div class="box-header">
              <h3 class="box-title"><b>Siswa</b></h3>
    </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>Nama Peminjam</th>
                  <th>Kelas</th>
                  <th>tgl Pinjam</th>
                  <th>tgl Kembali</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $n=1;
                foreach($peminjam as $p){ 
                ?>
                <tr>
                  <td><?php echo $n++?></td>
                  <td ><?php echo $p->nama_siswa?></td>
                  <td><?php echo $p->id_kejur?></td>
                  <td><?php echo $p->tgl_pinjam?></td>
                  <td><?php echo $p->tgl_kembali?></td>
                </tr>
              <?php }?>
                </tbody>
                <tfoot>
                <tr>
                  <th>NO</th>
                  <th>Nama Peminjam</th>
                  <th>Kelas</th>
                  <th>tgl Pinjam</th>
                  <th>tgl Kembali</th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>

              <div class="chart tab-pane" id="guru1">
              <div class="box-header">
              <h3 class="box-title"><b>Guru</b></h3>
    </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example3" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>Nama Peminjam</th>
                  <th>Jabatan</th>
                  <th>tgl Pinjam</th>
                  <th>tgl Kembali</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $n=1;
                foreach($gpeminjam as $gp){ 
                ?>
                <tr>
                  <td><?php echo $n++?></td>
                  <td ><?php echo $gp->nama?></td>
                  <td><?php echo $gp->jabatan?></td>
                  <td><?php echo $gp->tgl_pinjam?></td>
                  <td><?php echo $gp->tgl_kembali?></td>
                </tr>
              <?php }?>
                </tbody>
                <tfoot>
                <tr>
                  <th>NO</th>
                  <th>Nama Peminjam</th>
                  <th>Jabatan</th>
                  <th>tgl Pinjam</th>
                  <th>tgl Kembali</th>
                </tr>
                </tfoot>
              </table>
            </div>

              </div>
            </div>
          </div>
<script type="text/javascript">
   function tambahHobi() {
     var idf = document.getElementById("idf").value;
     var stre;
     var asr=1;
     stre="<p id='srow" + idf + "'><br><input type='text' required='' placeholder='--judul buku--' class='flexdatalist form-control' data-min-length='1' data-selection-required='true' list='buku" + asr + "' name='kode_buku[]'><datalist id='buku" + asr + "'><?php 
                              foreach($buku as $p){ 
                              ?>
                                <option><?php echo $p->judul?> (<?php echo $p->kode_buku?>)</option><?php }?>
                            </datalist><input type='number' name='jml[]' min='1' size='20' placeholder='--jumlah--'><a href='#' style=\"color:#3399FD;\" onclick='hapusElemen(\"#srow" + idf + "\"); return false;'> <button class='btn btn-danger'><i class='fa fa-minus'></i></button></a>";
     $("#divHobi").append(stre);
     idf = (idf-1) + 2;
     document.getElementById("idf").value = idf;
   }
   function hapusElemen(idf) {
     $(idf).remove();
   }
   function tambahBuku() {
     var idfg = document.getElementById("idfg").value;
     var stre;
     var asr=1;
     stre="<p id='srow" + idfg + "'><br><input type='text' required='' placeholder='--judul buku--' class='flexdatalist form-control' data-min-length='1' data-selection-required='true' list='buku" + asr + "' name='kode_buku[]'><datalist id='buku" + asr + "'><?php 
                              foreach($buku as $p){ 
                              ?>
                                <option><?php echo $p->judul?> (<?php echo $p->kode_buku?>)</option><?php }?>
                            </datalist><input type='number' name='jml[]' min='1' size='20' placeholder='--jumlah--'><a href='#' style=\"color:#3399FD;\" onclick='hapusElemeng(\"#srow" + idfg + "\"); return false;'> <button class='btn btn-danger'><i class='fa fa-minus'></i></button></a>";
     $("#divBuku").append(stre);
     idfg = (idfg-1) + 2;
     document.getElementById("idfg").value = idfg;
   }
   function hapusElemeng(idfg) {
     $(idfg).remove();
   }
</script>