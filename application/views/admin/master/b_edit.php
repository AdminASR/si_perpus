

<div class="col-sm-12">
	<div class="row">
   <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <a href="<?php echo base_url('master/buku')?>" class="btn btn-success"><i class="fa fa-reply"></i></a>
              <h3 class="box-title">Edit Data Buku</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php foreach($buku as $data){ ?>
            <form action="<?php echo base_url('master/updateb')?>" enctype="multipart/form-data" class="form-horizontal" method="post">
            
              <div class="box-body">
                <div class="col-sm-6">
               
                <div class="form-group">
                  <label class="col-sm-3">Judul</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="judul" placeholder="judul buku" value="<?php echo $data->judul ?>"  required >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Kategori</label>


                  <div class="col-sm-8">
                   <select name="kode_kategori" class="form-control" placeholder="" >
                    <option value="000" <?php if($data->kode_kategori == 000){echo 'selected';}?> >KARYA UMUM</option>
                    <option value="100" <?php if($data->kode_kategori == 100){echo 'selected';}?>>FILSAFAT</option>
                    <option value="200" <?php if($data->kode_kategori == 200){echo 'selected';}?>>AGAMA</option>
                    <option value="300" <?php if($data->kode_kategori == 300){echo 'selected';}?>>ILMU SOSIAL</option>
                    <option value="400" <?php if($data->kode_kategori == 400){echo 'selected';}?>>BAHASA</option>
                    <option value="500" <?php if($data->kode_kategori == 500){echo 'selected';}?>>ILMU - ILMU MURNI</option>
                    <option value="600" <?php if($data->kode_kategori == 600){echo 'selected';}?>>ILMU - ILMU TERAPAN</option>
                    <option value="700" <?php if($data->kode_kategori == 700){echo 'selected';}?>>SENI & OLAHRAGA</option>
                    <option value="800" <?php if($data->kode_kategori == 800){echo 'selected';}?>>SASTRA</option>
                    <option value="900" <?php if($data->kode_kategori == 900){echo 'selected';}?>>SEJARAH</option>
                    <option value="REF" <?php if($data->kode_kategori == 'REF'){echo 'selected';}?>>REFRENSI</option>
                  </select>
                  <small class="help-block">*pilih kategori</small>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Pengarang</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="pengarang" placeholder="pengarang" value="<?php echo $data->pengarang ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Penerbit</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="penerbit" placeholder="penerbit" value="<?php echo $data->penerbit ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Tahun Terbit</label>

                  <div class="col-sm-8">
                    <input type="number" class="form-control" name="thn_terbit" placeholder="yyyy" value="2000" value="<?php echo $data->thn_terbit ?>" required >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Kondisi</label>

                  <div class="col-sm-8">
                    <table>
                      <tr>
                        <td width="20px"><input <?php if($data->kondisi == 1){echo 'checked';}?> type="radio"  name="kondisi" value="1"></td>
                        <td ><label>Baik</label></td>
                        <td><div class="col-sm-3"></div></td>
                        <td width="20px"> <input <?php if($data->kondisi == 2){echo 'checked';}?> type="radio"  name="kondisi" value="2"></td>
                        <td><label>Tidak Baik</label></td>
                                  
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
                <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-sm-3">Lokasi</label>

                  <div class="col-sm-8">
                    <input type='text'
                                   placeholder="*kode rak"
                                   class='flexdatalist1 form-control'
                                   data-min-length='1'
                                   data-selection-required='true'
                                   list='rak'
                                   name='kode_rak' value="<?php echo $data->kode_rak ?>" >

                            <datalist id="rak">
                              <?php 
                              foreach($rak as $u){ 
                              ?>
                                <option ><?php echo $u->kode_rak ?></option>
                              <?php }?>
                            </datalist>
                  </div>
                </div>
                 
                <div class="form-group">
                  <label class="col-sm-3">Jumlah</label>

                  <div class="col-sm-8">
                    <input type="number" class="form-control" name="jumlah" placeholder="Jumlah Buku" value="<?php echo $data->jumlah ?>"  required>
                  </div>
                </div>
                 <div class="form-group">
                  <label class="col-sm-3">Asal</label>
         
                  <div class="col-sm-8">
                    <table>
                      <tr>
                        <td width="20px"><input <?php if($data->asal == 1){ echo'checked'; } ?> type="radio"  name="asal" value="1"></td>
                        <td width="20px"><label>Dropping</label></td>
                        <td><div class="col-sm-3"></div></td>
                        <td width="20px"> <input <?php if($data->asal == 2){ echo'checked'; } ?> type="radio"  name="asal" value="2"></td>
                        <td width="20px"><label>Hadiah</label></td>
                        <td><div class="col-sm-3"></div></td>
                        <td width="20px"><input <?php if($data->asal == 3){ echo'checked'; } ?> type="radio"  name="asal" value="3"></td>
                        <td width="20px"><label>Beli</label></td>
                      </tr>
                    </table>
                    
                    
                      
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Ringkasan</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" rows="2" name="deskripsi" placeholder="ringkasan buku" value="<?php echo $data->deskripsi ?>" ></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Foto</label>

                  <div class="col-sm-8">
                    <input type="file"  name="foto_buku">
                    <input type="text" name="buku_lama" value="<?php echo $data->foto?>">
                    <input type="hidden" name="status" value="<?php echo $data->status?>">
                    <input type="hidden" name="kode_buku" value="<?php echo $data->kode_buku?>">
                    <small class="help-block">*img/jpg/png</small>
                  </div>
                </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              <input type="submit" name="submit" value="Simpan" class="btn btn-primary pull-right">
              </div>
              <!-- /.box-footer -->
            </form>
          <?php }?>
          </div>
        </div>
    </div>
    