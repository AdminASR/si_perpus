<div class="col-sm-12">
  <div class="row">
   <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <a href="<?php echo base_url('master/guru')?>"><button class="btn btn-success pull-left"><i class="fa fa-mail-reply"></i></button></a>
              <h3 class="box-title">Edit Data Guru</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php foreach($guru as $g){ ?>
            <form class="form-horizontal" method="post" action="<?php echo base_url(). 'master/updateg'; ?>">
              <div class="box-body">

                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                <div class="form-group">
                  <label class="col-sm-3">NO Anggota</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="no_anggota" required="" placeholder="0876643465387" value="<?php echo $g->no_anggota?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Nama</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="nama" required="" placeholder="Nama Lengkap" value="<?php echo $g->nama?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Jabatan</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="jabatan"  placeholder="guru/kepsek/TU/dll" value="<?php echo $g->jabatan?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Alamat</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" rows="2" name="alamat" placeholder="Jalan, no rumah, Desa, Kecamatan, Kabupaten, Kode Pos " ><?php echo $g->alamat?></textarea>
                  </div>
                </div>
              </div>
                <div class="col-sm-2"></div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          <?php }?>
          </div>
        </div>
    </div>
<script language="JavaScript">
            <!-- fungsi alphabet -->
            function huruf(nilai, pesan) {
                var alphaExp = /^[a-zA-Z]+$/;
                if(nilai.value.match(alphaExp)) {
                    return true;
                }
            }
            <!-- fungsi number -->
            function angka(nilai, pesan) {
                var numberExp = /^[0-9]+$/;
                if(nilai.value.match(numberExp)) {
                    return true;
                }
            }
            <!-- fungsi Email  -->
            function email(nilai, pesan) {
                var email = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
                if(nilai.value.match(email)) {
                    return true;
                }
            }
        </script>
