
<div class="box">
    <div class="box-header">
              <h3 class="box-title"><i class="fa fa-users"></i> Data Siswa</h3>
              <a href="<?php echo base_url('master/form_tambah_siswa')?>"><button class="btn btn-success pull-right"><i class="fa fa-user-plus"></i>Tambah Data</button></a>
    </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>NISN</th>
                  <th>Nama</th>
                  <th>Jenis Kelamin</th>
                  <th>Kelas/Jurusan</th>
                  <th>Alamat</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                <?php 
                foreach($siswa as $u){ 
                ?>
                <tr>
                  <td><?php echo $u->nis?></td>
                  <td><?php echo $u->nama_siswa ?></td>
                  <td><?php if($u->jenis_kelamin=='L'){
                    echo 'Laki-laki';
                  }elseif ($u->jenis_kelamin=='P') {
                    echo 'Perempuan';
                  }else echo "-"; ?></td>
                  <td><?php echo $u->id_kejur ?></td>
                  <td><?php echo $u->alamat ?></td>
                  <td><center>
                    <?php echo anchor('master/edits/'.$u->nis,'<button type="button" class="btn btn-success"><i class="fa fa-edit"> Edit</i></button>'); ?>
                    <?php echo anchor('master/hapuss/'.$u->nis,'<button class="btn btn-danger"><i class="fa fa-trash"></i>Hapus</button>'); ?></center>
                  </td>
                </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>NISN</th>
                  <th>Nama</th>
                  <th>Jenis Kelamin</th>
                  <th>Kelas/Jurusan</th>
                  <th>Alamat</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
</div>