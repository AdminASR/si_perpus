
<div class="box">
    <div class="box-header">
              <h3 class="box-title"><i class="fa fa-users"></i> Data Guru</h3>
              <a href="<?php echo base_url('master/form_tambah_guru')?>"><button class="btn btn-success pull-right"><i class="fa fa-user-plus"></i>Tambah Data</button></a>
    </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>No Anggota</th>
                  <th>Nama</th>
                  <th>Jabatan</th>
                  <th>Alamat</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                <?php 
                foreach($guru as $g){ 
                ?>
                <tr>
                  <td><?php echo $g->no_anggota?></td>
                  <td><?php echo $g->nama ?></td>
                  <td><?php echo $g->jabatan ?></td>
                  <td><?php echo $g->alamat ?></td>
                  <td><center>
                    <?php echo anchor('master/editg/'.$g->no_anggota,'<button type="button" class="btn btn-success"><i class="fa fa-edit"> Edit</i></button>'); ?>
                    <?php echo anchor('master/hapusg/'.$g->no_anggota,'<button class="btn btn-danger"><i class="fa fa-trash"></i>Hapus</button>'); ?></center>
                  </td>
                </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No Anggota</th>
                  <th>Nama</th>
                  <th>Jabatan</th>
                  <th>Alamat</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
</div>
