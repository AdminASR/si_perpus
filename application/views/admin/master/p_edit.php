<div class="col-sm-12">
	<div class="row">
   <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <a href="<?php echo base_url('master/pegawai')?>"><button class="btn btn-success pull-left"><i class="fa fa-mail-reply"></i></button></a>
              <h3 class="box-title">Edit data Pegawai</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php 
             foreach($siswa as $u){ 
            ?>                 
            <form class="form-horizontal" method="post" action="<?php echo base_url(). 'master/updatep'; ?>">
              <div class="box-body">
                <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-sm-3">NIS</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="nis" required="" value="<?php echo $u->nis ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Nama</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="nama_siswa" required="" value="<?php echo $u->nama_siswa ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Jenis Kelamin</label>

                  <div class="col-sm-8">
                    <input type="radio" <?php if ($u->jenis_kelamin=='L'){ echo "checked";}?>  name="jenis_kelamin" id="jenis_kelamin1" value="L" required="" >Laki-laki
                    <input type="radio" <?php if ($u->jenis_kelamin=='P'){ echo "checked";}?> name="jenis_kelamin" id="jenis_kelamin1" value="P" required="" >Perempuan
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Tempat Lahir</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="tempat_lahir" required="" value="<?php echo $u->tempat_lahir ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">TGL Lahir</label>

                  <div class="col-sm-8">
                    <input type="date" class="form-control" name="tgl_lahir" required="" value="<?php echo $u->tgl_lahir ?>">
                  </div>
                </div>
            	</div>
                <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-sm-3">Alamat</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" rows="2" name="alamat" required=""><?php echo $u->alamat ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Kelas</label>

                  <div class="col-sm-4">
                        <input type='text'
                               placeholder='--kelas--'
                               class='flexdatalist form-control'
                               data-min-length='1'
                               data-selection-required='true'
                               list='tes'
                               name='id_kejur'
                               value="<?php echo $u->id_kejur ?>">

                            <datalist id="tes">
                              <?php 
                              foreach($kelas as $u){ 
                              ?>
                                <option ><?php echo $u->id_kejur ?></option>
                              <?php }?>
                            </datalist>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Email</label>

                  <div class="col-sm-8">
                    <input type="email" class="form-control" name="email" required="" value="<?php echo $u->email ?>">
                  </div>
                </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          <?php }?>
          </div>
        </div>
    </div>
