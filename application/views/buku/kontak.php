<style>
/* The container */
.container_1 {

  position: relative;
  padding-left: 35px;
  padding-right: 20px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 15px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default radio button */
.container_1 input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}

/* Create a custom radio button */
.checkmark_1 {
  position: absolute;
  top: 0;
  left: 0;
  height: 20px;
  width: 20px;
  background-color: #eee;
  border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.container_1:hover input ~ .checkmark_1 {
  background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.container_1 input:checked ~ .checkmark_1 {
  background-color: #2196F3;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark_1:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the indicator (dot/circle) when checked */
.container_1 input:checked ~ .checkmark_1:after {
  display: block;
}

/* Style the indicator (dot/circle) */
.container_1 .checkmark_1:after {
 	top: 7px;
	left: 7px;
	width: 6px;
	height: 6px;
	border-radius: 50%;
	background: white;
}
</style>
	<!-- BREADCRUMB -->
	<div id="breadcrumb">
		<div class="container">
			<ul class="breadcrumb">
				<li><a href="index.html">Home</a></li>
				<li class="active">Kontak</li>
			</ul>
		</div>
	</div>
	<!-- /BREADCRUMB -->

	<!-- section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<form id="checkout-form" class="clearfix" action="<?php echo base_url('buku/tambah_komen')?>" method="post">
					<div class="col-md-12">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Tanggapan terhadap pelayanan perpustakaan</h3>
							</div>
							<br>
							<div class="form-group">
								 <div id="notifications" ><?php echo $this->session->flashdata('msg'); ?></div>
								<input class="input" type="hidden" name="tanggal" value="<?php echo date('Y-m-d')?>" >
							</div>
								<div class="form-group">
								<center>
									<h3>Rating</h3>
									<br>

								<label class="container_1">Sangat Buruk <input type="radio"  name="rating" value="1"><span class="checkmark_1"></span></label>
								<label class="container_1">Buruk <input type="radio"  name="rating" value="2"><span class="checkmark_1"></span></label>
								<label class="container_1">Cukup <input type="radio"  name="rating" value="3"><span class="checkmark_1"></span></label>
								<label class="container_1">Baik <input type="radio"  name="rating" value="4"><span class="checkmark_1"></span></label>
								<label class="container_1">Sangat Baik <input type="radio"  name="rating" value="5"><span class="checkmark_1"></span></label>
								</center>
								<br>
								<br>
							
								

							

								
							</div>
							<div class="form-group">
								<textarea  class="input-1" name="komentar" placeholder="kritik dan saran" ></textarea>
							</div>

							<div class="form-group">
								<div class="pull-right">
								<input type="submit" name="simpan" value="kirim" class="btn btn-primary">
							</div>
							</div>
					</div>
				</div>
			</form>



					



			<!-- /row -->
		</div>
		<div class="row">
				<!-- section title -->
				<div class="col-md-12">
					<div class="section-title">
						<h2 class="title">data pegawai</h2>
					</div>
				</div>
				<!-- section title -->

				<!-- Product Single -->
				
			<?php
if( ! empty($pegawai)){ // Jika data pada database tidak sama dengan empty (alias ada datanya)
  foreach($pegawai as $data){ ?> 




  	<div class="col-md-4 col-sm-6 col-xs-6">
				<div class="product product-single">
								<div class="product-thumb">
									
									
									
									<?php if ($data->jenis_kelamin=='L') {?>
										<img src="<?php echo base_url('assets_2/img/male.png')?>" alt="">
									<?php }elseif ($data->jenis_kelamin=='P') {?>
										<img src="<?php echo base_url('assets_2/img/female.png')?>" alt="">
									<?php }?>
								</div>
								<div>
									<table align="center">
										<tr>
											<td align="center"><p><b><h4><?php echo $data->nama_pegawai ?></h4></b></p></td>
										</tr>
										<tr>
											<td align="center"><b>(<?php if($data->jabatan=='1'){
                    echo 'Kepala Perpustakaan';
                  }elseif ($data->jabatan=='2') {
                    echo 'Pustakawan';
                  } ?>)</b></td>
										</tr>
										<tr>
											<td align="center"><b><?php echo $data->telp ?></b></td>
										</tr>
									</table>
									<br>
								</div>
							</div>
				</div>
    

              <!-- php -->
              <?php  }
}
?>
				</div>
			</div>
		<!-- /container -->
	</div>
	<!-- /section -->
	<!-- /section -->

	<!-- FOOTER -->
		