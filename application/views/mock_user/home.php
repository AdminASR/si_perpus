


  <!-- HOME -->



  <div class="section section-grey">
    <div class="container">
      <div class="row">
        <div class="col-md-8" id="home-slick">
           <div class="banner banner-2">
            <img src="<?php echo base_url('images/mock_up/buku-hatta.jpg'); ?>" alt=""  height="525px" >
          </div>
         <!--  <div class="banner banner-2">
            <img src="<?php echo base_url('images/mock_up/300400pxxx_7675_Georeferencing_Menggunakan_ArcGIS_10_1___CD__.jpg'); ?>" alt="" height="525px">
          </div>
          <div class="banner banner-2">
             <img src="<?php echo base_url('images/mock_up/muri_6648_Teknik_Memasang_Chatbot_di_Toko_Online_.jpg'); ?>" alt="" height="525px">
          </div>
          <div class="banner banner-2">
             <img src="<?php echo base_url('images/mock_up/Analisis-f.jpg'); ?>" alt="" height="525px">
          </div>
           <div class="banner banner-2">
             <img src="<?php echo base_url('images/mock_up/muri_6650_Buku_Sakti_Pemrograman_Web__HTML__CSS__PHP__MySQL___Javascript__.jpg'); ?>" alt="" height="525px">
          </div>
           <div class="banner banner-2">  
          <img src="<?php echo base_url('images/mock_up/300400pxxx_7742_Pengantar_Sejarah_dan_Konsep_Estetika__.jpg'); ?>" alt="" height="525px">
          </div>
           <div class="banner banner-2">
            <img src="<?php echo base_url('images/mock_up/300400pxxx_7617_Seni_Rupa_Modern_Edisi_Revisi__.jpg'); ?>" alt="" height="525px">
          </div>
           <div class="banner banner-2">
            <img src="<?php echo base_url('images/mock_up/300x450_6815_Street_Photography__Jurus_Sakti_Fotografi_Jalanan_Terlengkap__.jpg'); ?>" alt="" height="525px">
          </div>
           <div class="banner banner-2">
            <img src="<?php echo base_url('images/mock_up/muri_6650_Buku_Sakti_Pemrograman_Web__HTML__CSS__PHP__MySQL___Javascript__.jpg'); ?>" alt="" height="525px">
          </div>
           <div class="banner banner-2">
           <img src="<?php echo base_url('images/mock_up/300400pxxx_7746_Paradigma_Pendidikan_Seni.jpg'); ?>" alt="" height="525px">
          </div> -->
        </div>
      
        <p></p>

        <div class="col-md-4 col-sm-6" id="home-slick">
          <a class="banner banner-1" href="<?php echo base_url('event/detail/');?>" align="center">
            <img src="<?php echo base_url('images/mock_up/h.jpg')?>" alt="">
            <div class="banner-caption text-center">
              <h2 class="white-color"></h2>
            </div>
          </a>
        </div>
        <br>
        <br>
         <div class="col-md-4 col-sm-6">
          <a class="banner banner-1" href="<?php echo base_url('event/detail/');?>" align="center">
            <img src="<?php echo base_url('images/mock_up/i.jpg')?>" alt="">
            <div class="banner-caption text-center">
              <h2 class="white-color"></h2>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>



  <div class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="section-title">
            <h2 class="title">Buku Terpopuler</h2>
            <div class="pull-right">
              <div class="product-slick-dots-1 custom-dots"></div>
            </div>
          </div>

          <div class="row">
            <div id="product-slick-1" class="product-slick">
              <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/333px_8516_Presentasi_yang_Mencekam_Edisi_Revisi__.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Presentasi yang Mencekam Edisi Revisi</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2002)Sutanto L. Tjokro</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Komputer & Teknologi</b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
              <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/300400pxxx_7675_Georeferencing_Menggunakan_ArcGIS_10_1___CD__.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4> Georeferencing Menggunakan ArcGIS 10.1 + CD</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2018)Andi Offset</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Komputer & Teknologi</b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
              <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/Analisis-f.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Computer Graphic Design Edisi Revisi 3</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2017)Informatika</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Komputer & Teknologi</b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
              <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/muri_6648_Teknik_Memasang_Chatbot_di_Toko_Online_.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Teknik Memasang Chatbot Di Toko Online</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2018)Elex Media Komputindo</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Komputer & Teknologi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
               <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/muri_6650_Buku_Sakti_Pemrograman_Web__HTML__CSS__PHP__MySQL___Javascript__.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Buku Sakti Pemrograman Web: HTML, CSS, PHP, MySQL & Javascript</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2018)Start Up</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Komputer & Teknologi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
                 <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/300400pxxx_7742_Pengantar_Sejarah_dan_Konsep_Estetika__.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Pengantar Sejarah dan Konsep Estetika</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2015)Kanisius</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Seni & Fotografi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
                 <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/300400pxxx_7617_Seni_Rupa_Modern_Edisi_Revisi__.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Seni Rupa Modern Edisi Revisi</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2012)Rekayasa Sains Bandung</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Seni & Fotografi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
                 <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/300x450_6815_Street_Photography__Jurus_Sakti_Fotografi_Jalanan_Terlengkap__.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Street Photography: Jurus Sakti Fotografi Jalanan Terlengkap</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2016)Grasindo</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Seni & Fotografi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
                 <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/muri_6650_Buku_Sakti_Pemrograman_Web__HTML__CSS__PHP__MySQL___Javascript__.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Seni Belajar Grafologi Edisi Baru</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2013)Laksana</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Seni & Fotografi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
                 <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/300400pxxx_7746_Paradigma_Pendidikan_Seni.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Paradigma Pendidikan Seni</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2017)Thafa Media</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Seni & Fotografi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /Product Slick -->
      
        <!-- section-title -->
      <div class="col-md-6">
          <div class="section-title">
            <h2 class="title">Rekomendasi untuk anda</h2>
            <div class="pull-right">
              <div class="product-slick-dots-2 custom-dots"></div>
            </div>
          </div>
        
 





          <div class="row">
            <div id="product-slick-2" class="product-slick">
                <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/300x450_6815_Street_Photography__Jurus_Sakti_Fotografi_Jalanan_Terlengkap__.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Street Photography: Jurus Sakti Fotografi Jalanan Terlengkap</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2016)Grasindo</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Seni & Fotografi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
                 <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/muri_6650_Buku_Sakti_Pemrograman_Web__HTML__CSS__PHP__MySQL___Javascript__.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Seni Belajar Grafologi Edisi Baru</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2013)Laksana</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Seni & Fotografi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
                 <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/300400pxxx_7746_Paradigma_Pendidikan_Seni.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Paradigma Pendidikan Seni</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2017)Thafa Media</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Seni & Fotografi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>



      <div class="row">
        <div class="col-md-6">
          <div class="section-title">
            <h2 class="title">DENAH</h2> 
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="product product-single">
                <div class="product-thumb">
                  <a href="<?php echo base_url('assets_2/img/denah1.jpg')?>" class="main-btn quick-view"> <i class="fa fa-search-plus"></i> Detail</a>
                 <img src="<?php echo base_url('assets_2/img/denah2.jpg')?>" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="section-title">
            <h2 class="title">INFORMASI</h2>
            <h5 class="">(centang checkbox untuk lihat info)</h5>       
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="input-checkbox">
                <input type="checkbox" name="shipping" id="shipping-1" class="pull-right" >
                <label for="shipping-1">Bagaimana Cara Meminjam Buku   ?</label>
                <div class="caption">
                  <p> <strong>1.</strong>&nbsp;Siswa Menyerahkan Kartu Anggota Perpustakaan </p>
                  <p><strong>2.</strong>&nbsp;Siswa Mencari buku yang akan dipinjam melalui Katalog</p>
                  <p><strong>3.</strong>&nbsp; Siswa Mengisi Formulir Peminjaman Buku, Tugas Akhir, atau Kuliah Kerja Praktek</p>
                  <p><strong>4.</strong>&nbsp;Petugas memberikan stempel tanda tanggal pengembalian buku yang akan di pinjam</p>
                  <p><strong>5.</strong>&nbsp;Petugas menyerahkan buku</p>
                  <p><strong>6.</strong>&nbsp;Maksimal lama peminjaman buku selama 3 hari</p>
                  <p><strong>7.</strong>&nbsp;Maksimal Perpanjangan Peminjaman buku sebanyak 3 kali</p>
                </div>
              </div>

              <div class="input-checkbox">
                <input type="checkbox" name="shipping" id="shipping-2" class="pull-right" >
                <label for="shipping-2">Jam Berapa Perpustakaan Beroperasi  ?</label>
                <div class="caption">
                  <p><strong>1.</strong>&nbsp;Senin (08.00–14.00)</p>
                  <p><strong>2.</strong>&nbsp;Selasa(08.00–14.00)</p>
                  <p><strong>3.</strong>&nbsp;Rabu  (08.00–14.00)</p>
                  <p><strong>4.</strong>&nbsp;Kamis (08.00–14.00)</p>
                  <p><strong>5.</strong>&nbsp;Jum'at(08.00–14.00)</p>
                </div>
              </div>
              <div class="input-checkbox">
                <input type="checkbox" name="shipping" id="shipping-3" class="pull-right" >
                <label for="shipping-3">Bolehkah Pinjam Lebih Dari Satu(1) Buku  ?</label>
                <div class="caption">
                  <p><strong>1.</strong>&nbsp;Maksimal peminjaman buku sebanyak 2 buku</p>
                </div>
              </div>
              <div class="input-checkbox">
                <input type="checkbox" name="shipping" id="shipping-4" class="pull-right" >
                <label for="shipping-4">Apa Sanksi Jika Telat Mengembalikan ?</label>
                <div class="caption">
                  <p><strong>1.</strong>&nbsp;Sangsi keterlambatan pengembalian buku pinjaman sebesar Rp.1.000 (Seribu Rupiah)</p>
                  <p><strong>2.</strong>&nbsp;Sangsi jika buku hilang atau rusak diganti dengan buku yang sama</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Detail Buku</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <h2 class="title">Foto</h2>
              <div class="row">
                <div class="col-md-12">
                  <table>
                    <tr>
                        <td><img src="<?php echo base_url('images/mock_up/muri_6650_Buku_Sakti_Pemrograman_Web__HTML__CSS__PHP__MySQL___Javascript__.jpg'); ?>" width="300px" height="400px" ></td>
                        <td><b><span id="val_foto1"></span></b></td>
                    </tr>
                    </table>
                    <br>
                    <br> 
                </div>
              </div>
            </div>

          <div class="col-md-6">
              <h2 class="title">Keterangan</h2>
              <div class="row">
                <div class="col-md-12">
                  <table >
                    <tr>
                      <td width="20%"><b>Judul</b></td>
                      <td width="5%">:</td>
                      <td><b><span id="val_judul"></span>Buku Sakti Pemrograman Web: HTML, CSS, PHP, MySQL & Javascript</b></td>
                    </tr>
                      <tr>
                      <td width="20%"><b>Kode Rak</b></td>
                      <td width="5%">:</td>
                      <td><b><span id="val_kode_rak">900</span></b></td>
                    </tr>
                    <tr>
                      <td width="20%">Kode Buku</td>
                      <td width="5%">:</td>
                      <td><span id="val_kode_buku">REF001</span></td>
                    </tr>
                    <tr>
                      <td width="20%">Kategori</td>
                      <td width="5%">:</td>
                      <td><span id="val_kode_kategori">Komputer & Teknologi</span></td>
                    </tr>
                      <tr>
                      <td>Pengarang</td>
                      <td>:</td>
                      <td><span id="val_pengarang">Didik Setiawan</span></td>
                    </tr>
                    <tr>
                      <td>Penerbit</td>
                      <td>:</td>
                      <td><span id="val_penerbit">Start Up</span></td>
                    </tr>
                     <tr>
                      <td>Tahun Terbit</td>
                      <td>:</td>
                      <td><span id="val_thn_terbit">2018</span></td>
                    </tr>
                    <tr>
                      <td>Jumlah</td>
                      <td>:</td>
                      <td><span id="val_jumlah">9</span></td>
                    </tr>
                    <tr>
                      <td>Deskripsi</td>
                      <td>:</td>
                      <td><span id="val_deskripsi">Buku Sakti Pemrograman Web: HTML, CSS, PHP, MySQL & Javascript</span></td>
                    </tr>
                   </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

