
	<!-- BREADCRUMB -->
	<div id="breadcrumb">
		<div class="container">
			<ul class="breadcrumb">
				<li><a href="index.html">Home</a></li>
				<li class="active">Event</li>
			</ul>
		</div>
	</div>
	<!-- /BREADCRUMB -->

	<!-- section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- section title -->
				<div class="col-md-12">
					<div class="section-title">
						<h2 class="title">Halaman Event</h2>
					</div>
				</div>

				  <div class="col-md-4">
          	 <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                 <img src="<?php echo base_url('images/mock_up/h.jpg')?>" alt="">
                </div>
                <div>
                  <table align="center">
										<tr>
											<td align="center"><p><b><h4>Seminar Buku Jendela Dunia</h4></b></p></td>
										</tr>
										<tr>
											<td align="center"><b>(21-12-2019)</b></td>
										</tr>
										
									</table>
                </div>
              </div>
          </div>
              	  <div class="col-md-4">
          	 <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/i.jpg')?>" alt="">
                </div>
                <div>
                 	<table align="center">
										<tr>
											<td align="center"><p><b><h4>Seminar Bulan Bahasa</h4></b></p></td>
										</tr>
										<tr>
											<td align="center"><b>(23-02-2018)</b></td>
										</tr>
									
									</table>
                </div>
              </div>
          </div>
						
			
			</div>

	
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Detail Buku</h4>
        </div>
        <div class="modal-body">
         
        <div class="row">
				<!-- section-title -->

					<div class="col-md-6">
					
						<h2 class="title">Foto</h2>
						
					
					<div class="row">
						<div class="col-md-12">
							
		 <table >
         
         	<tr>
         		
         		<td> <img src="<?php echo base_url('images/mock_up/h.jpg')?>" alt="" width="400px"></td>
         	</tr>
         </table>
         <br>
		<br>
		</div>
	</div>
</div>
				
				<!-- /Product Slick -->
			
				<!-- section-title -->
				<div class="col-md-6">
					
						<h2 class="title">Keterangan</h2>
						
					
					<div class="row">
						<div class="col-md-12">
							
		 <table >
         
         	<tr>
         		<td width="20%">Judul</td>
         		<td width="5%">:</td>
         		<td><b>Seminar Buku Jendela Dunia</b> </td>
         	</tr>
         	<tr>
         		<td>Tanggal</td>
         		<td>:</td>
         		<td><b>21-12-2019</b> </td>
         	</tr>
         	<tr>
         		<td>Tempat</td>
         		<td>:</td>
         		<td>SMKN 6 Jember</td>
         	</tr>
         	
         	<tr>
         		<td>Deskripsi</td>
         		<td>:</td>
         		<td>Seminar Bulan Bahasa</td>
         	</tr>
         	
         </table>
						</div>
						
					</div>
				</div>
				<!-- /Product Slick -->
			</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

			
			
		</div>
		<!-- /container -->
	</div>
	<!-- /section -->

	