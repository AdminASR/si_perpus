

  <!-- FOOTER -->
  <footer id="footer" class="section section-grey">
    <!-- container -->
    <div class="container">
      <!-- row -->
      <div class="row">
        <!-- footer widget -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="footer">
            <!-- footer logo -->
            <div class="footer-logo">
              <a class="logo" href="#">
                <img src="<?php echo base_url('assets_2/img/smk.png')?>" alt="">
              </a>
            </div>
            <!-- /footer logo -->

            <p>Program ini dibuat untuk mempermudah siswa dalam mendapatkan informasi buku ,mencari buku  dan meminjam buku.</p>

            <!-- footer social -->
            <ul class="footer-social">
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              
            </ul>
            <!-- /footer social -->
          </div>
        </div>
        <!-- /footer widget -->

        <!-- footer widget -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="footer">
            <h3 class="footer-header">Menu</h3>
            <ul class="list-links">
              <li><a href="index.html">HOME</a></li>
            <li><a href="katalog.html">KATALOG</a></li>   
            
            <li><a href="event.html">EVENT</a></li>       
            </ul>
          </div>
        </div>
        <!-- /footer widget -->

        <div class="clearfix visible-sm visible-xs"></div>

        <!-- footer widget -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="footer">
            <h3 class="footer-header">Kritik dan saran</h3>
            <ul class="list-links">
              <li><a href="kontak.html">KONTAK</a></li>
            </ul>
          </div>
        </div>
        <!-- /footer widget -->

        <!-- footer subscribe -->
        
        <!-- /footer subscribe -->
      </div>
      <!-- /row -->
      <hr>
      <!-- row -->
      <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center">
          <!-- footer copyright -->
          <div class="footer-copyright">
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> Dafidea Technocraft by <a href="https://colorlib.com" target="_blank">Faisal'ID & ASR</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          </div>
          <!-- /footer copyright -->
        </div>
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </footer>
  <!-- /FOOTER -->



</body>
  <!-- jQuery Plugins -->

 <script>
    function tampildata(kode_buku,kode_rak, kode_kategori,judul, pengarang, penerbit, thn_terbit, jumlah, deskripsi, foto){
      if($('#val_foto_1').html(foto)){
         $('#val_foto_2').attr('src', '../../images/buku/'+foto);
       }else if($('#val_foto_1').html(foto =='-')){

         $('#val_foto_2').attr('src', '../../images/buku/default.png');        
       }


      $('#val_kode_buku').html(kode_buku);
      $('#val_kode_rak').html(kode_rak);
      $('#val_kode_kategori').html(kode_kategori);
      $('#val_judul').html(judul);
      $('#val_pengarang').html(pengarang);
      $('#val_penerbit').html(penerbit);
      $('#val_thn_terbit').html(thn_terbit);
      $('#val_jumlah').html(jumlah);
      $('#val_deskripsi').html(deskripsi);

      
     
      $('#val_foto').attr('src', '../images/buku/'+foto);
   
    }
  </script>
  <script src="<?php echo base_url('assets_2/js/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets_2/js/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('assets_2/js/slick.min.js')?>"></script>
  <script src="<?php echo base_url('assets_2/js/nouislider.min.js')?>"></script>
  <script src="<?php echo base_url('assets_2/js/jquery.zoom.min.js')?>"></script>
  <script src="<?php echo base_url('assets_2/js/main.js')?>"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
var closebtns = document.getElementsByClassName("close");
var i;

for (i = 0; i < closebtns.length; i++) {
  closebtns[i].addEventListener("click", function() {
    this.parentElement.style.display = 'none';
  });
}
</script>


<!-- menu sort -->
<script>

function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

window.onclick = function(event) {
  if (!event.target.matches('.input')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>
<!-- /menu sort -->

</html>
