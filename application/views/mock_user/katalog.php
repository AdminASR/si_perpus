	<!-- BREADCRUMB -->
	<div id="breadcrumb">
		<div class="container">
			<ul class="breadcrumb">
				<li><a href="index.html">Home</a></li>
				<li class="active">Katalog</li>
			</ul>
		</div>
	</div>
	<!-- /BREADCRUMB -->
	<!-- section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<div id="aside" class="col-md-3">
				<form action="<?php echo base_url('buku/hasil_filter')?>" method="GET">				<!-- ASIDE -->
				
					<!-- aside widget -->
					<div class="aside">
						<h3 class="aside-title">berdasarkan Kategori </h3>
						<!-- <ul class="list-links">
						
							<li><input type="checkbox" name="karya_umum"  /> Karya Umum</li>
							<li><input type="checkbox" name="filsafat" /> Filsafat</li>
							<li><input type="checkbox" name="agama" /> Agama</li>
							<li><input type="checkbox" name="ilmu_sosial" /> Ilmu Sosia;</li>
							<li><input type="checkbox" name="bahasa" /> Bahasa</li>
							<li><input type="checkbox" name="ilmu_murni" /> Ilmu- Ilmu Murni</li>
							<li><input type="checkbox" name="ilmu_terapan" /> Ilmu terapan</li>
							<li><input type="checkbox" name="seni_olahraga" /> Seni & olah raga</li>
							<li><input type="checkbox" name="sastra" /> sastra</li>
							<li><input type="checkbox" name="sejarah" /> sejarah</li>

						</ul> -->

					
					
					<table>
								<tr>
									
									<td width="350px" ><ul class="list-links">
						
							<li><input type="radio" name="voss" value="000" /> Karya Umum</li>
							<li><input type="radio" name="voss" value="100"> Filsafat</li>
							<li><input type="radio" name="voss" value="200"/> Agama</li>
							<li><input type="radio" name="voss" value="300"/> Ilmu SosiaL</li>
							<li><input type="radio" name="voss" value="400"/> Bahasa</li>
							<li><input type="radio" name="voss" value="500"/> Ilmu- Ilmu Murni</li>
							<li><input type="radio" name="voss" value="600"/> Ilmu terapan</li>
							<li><input type="radio" name="voss" value="700"/> Seni & olah raga</li>
							<li><input type="radio" name="voss" value="800"/> sastra</li>
							<li><input type="radio" name="voss" value="900"/> sejarah</li>
							<li><input type="radio" name="voss" value="REF"/> Refrensi</li>
							<li><input type="radio" name="voss" value="REF"/> Komputer & Teknologi</li>
							<li><input type="radio" name="voss" value="REF"/> Seni & Fotografi</li>


						</ul></td>
				</tr>
				<tr>

									
									
								</tr>
							</table>
							<br>
							
					</div>
					<!-- /aside widget -->

					<!-- aside widget -->
					<div class="aside">
						
						<h3 class="aside-title">berdasarkan tahun</h3>
						<table>
							<tr><td align="center" colspan="3">Dari Tahun :</td></tr>
								<tr>
									
									<td width="150px" ><input class="input search-input"  type="number" placeholder="Tahun"  name="thn1"></td>
									<td>S/D</td>
									<td width="150px" ><input class="input search-input"  type="number" placeholder="Tahun"  name="thn2"></td></tr>

							</table>
							<br>
							<center><button class="btn btn-default" type="submit">Terapkan</button></center>
							
						
						</form>
					</div>
					<!-- aside widget -->
				</div>
				

				<!-- /ASIDE -->

				<!-- MAIN -->
				<div id="main" class="col-md-9">
					<!-- store top filter -->
					<div class="store-filter clearfix">
						<div class="pull-left">
							
							

<div class="dropdown">
  <button onclick="myFunction()" class="input">Sort By :</button>
  <div id="myDropdown" class="dropdown-content">
      <a href="<?php echo base_url('buku/asc')?>">A - Z</a>
     <a href="<?php echo base_url('buku/desc')?>">Z - A</a>
  </div>
</div>	
								
							
						</div>
						<div class="pull-right">
							<div >
								<div>
						<form action="<?php echo base_url('buku/hasil')?>" method="GET">
							<table>
								<tr>
									
									<td width="300px" ><input class="input search-input"  type="text" placeholder="Masukkan Judul"  id="cari" name="cari"></td>

									<td><button class="input" type="submit"><i class="fa fa-search"></i></button></td>
									
								</tr>
							</table>
						</form>

					</div>
				</div>
			</div>
		</div>
					<!-- /store top filter -->

					<!-- STORE -->


					<div id="store">

			



					

						<div class="row">
              

    	<div class="col-md-3">
    	  <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/333px_8516_Presentasi_yang_Mencekam_Edisi_Revisi__.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Presentasi yang Mencekam Edisi Revisi</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2002)Sutanto L. Tjokro</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Komputer & Teknologi</b></td>
                    </tr>
                  </table><br>
               </div>
            </div>
          </div>
          <div class="col-md-3">
          	 <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/300400pxxx_7675_Georeferencing_Menggunakan_ArcGIS_10_1___CD__.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4> Georeferencing Menggunakan ArcGIS 10.1 + CD</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2018)Andi Offset</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Komputer & Teknologi</b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
          </div>
             <div class="col-md-3">
             	 <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/Analisis-f.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Computer Graphic Design Edisi Revisi 3</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2017)Informatika</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Komputer & Teknologi</b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
             </div>
             <div class="col-md-3">
             	  <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/muri_6648_Teknik_Memasang_Chatbot_di_Toko_Online_.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Teknik Memasang Chatbot Di Toko Online</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2018)Elex Media Komputindo</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Komputer & Teknologi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
             </div>
            <div class="col-md-3">
            	<div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/muri_6650_Buku_Sakti_Pemrograman_Web__HTML__CSS__PHP__MySQL___Javascript__.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Buku Sakti Pemrograman Web: HTML, CSS, PHP, MySQL & Javascript</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2018)Start Up</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Komputer & Teknologi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
            </div>
            <div class="col-md-3">
            	 <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/300400pxxx_7742_Pengantar_Sejarah_dan_Konsep_Estetika__.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Pengantar Sejarah dan Konsep Estetika</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2015)Kanisius</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Seni & Fotografi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
            </div>
             <div class="col-md-3">
            	  <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/300400pxxx_7746_Paradigma_Pendidikan_Seni.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Paradigma Pendidikan Seni</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2017)Thafa Media</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Seni & Fotografi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
            </div>
            <div class="col-md-3">  
              <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/300400pxxx_7617_Seni_Rupa_Modern_Edisi_Revisi__.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Seni Rupa Modern Edisi Revisi</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2012)Rekayasa Sains Bandung</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Seni & Fotografi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
          </div>
            <div class="col-md-3">
            	  <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/300x450_6815_Street_Photography__Jurus_Sakti_Fotografi_Jalanan_Terlengkap__.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Street Photography: Jurus Sakti Fotografi Jalanan Terlengkap</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2016)Grasindo</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Seni & Fotografi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
            </div>
            <div class="col-md-3">
            	 <div class="product product-single">
                <div class="product-thumb">
                  <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata()"><i class="fa fa-search-plus"></i> Detail
                  </button>
                  <img src="<?php echo base_url('images/mock_up/muri_6650_Buku_Sakti_Pemrograman_Web__HTML__CSS__PHP__MySQL___Javascript__.jpg'); ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4>Seni Belajar Grafologi Edisi Baru</h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(2013)Laksana</b></td>
                    </tr>
                    <tr>
                      <td align="center"><b>Seni & Fotografi </b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
            </div>
           
               
                
               
               
                
               
   
          </div>

					</div>
					<br>

					<!-- /STORE -->

					<!-- store bottom filter -->
					<div class="store-filter clearfix">
						<div class="pull-left">
							<div class="sort-filter">
								<span class="text-uppercase">Sort By:</span>
								<select class="input">
								<option value="1">A - Z</option>
								<option value="1">Z - A</option>
									</select>
								<a href="#" class="main-btn icon-btn"><i class="fa fa-arrow-down"></i></a>
							</div>
						</div>
						<div class="pull-right">
							<div class="page-filter">
								<ul class="pagination">
  <li><a href="#">1</a></li>
  <li class="active"><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
</ul>
							</div>
							<ul class="store-pages">
								
							</ul>
						</div>
					</div>
					<!-- /store bottom filter -->
				</div>
				<!-- /MAIN -->
			</div>
			<!-- /row -->
		</div>
 <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Detail Buku</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <h2 class="title">Foto</h2>
              <div class="row">
                <div class="col-md-12">
                  <table>
                    <tr>
                        <td><img src="<?php echo base_url('images/mock_up/muri_6650_Buku_Sakti_Pemrograman_Web__HTML__CSS__PHP__MySQL___Javascript__.jpg'); ?>" width="300px" height="400px" ></td>
                        <td><b><span id="val_foto1"></span></b></td>
                    </tr>
                    </table>
                    <br>
                    <br> 
                </div>
              </div>
            </div>

          <div class="col-md-6">
              <h2 class="title">Keterangan</h2>
              <div class="row">
                <div class="col-md-12">
                  <table >
                    <tr>
                      <td width="20%"><b>Judul</b></td>
                      <td width="5%">:</td>
                      <td><b><span id="val_judul"></span>Buku Sakti Pemrograman Web: HTML, CSS, PHP, MySQL & Javascript</b></td>
                    </tr>
                      <tr>
                      <td width="20%"><b>Kode Rak</b></td>
                      <td width="5%">:</td>
                      <td><b><span id="val_kode_rak">900</span></b></td>
                    </tr>
                    <tr>
                      <td width="20%">Kode Buku</td>
                      <td width="5%">:</td>
                      <td><span id="val_kode_buku">REF001</span></td>
                    </tr>
                    <tr>
                      <td width="20%">Kategori</td>
                      <td width="5%">:</td>
                      <td><span id="val_kode_kategori">Komputer & Teknologi</span></td>
                    </tr>
                      <tr>
                      <td>Pengarang</td>
                      <td>:</td>
                      <td><span id="val_pengarang">Didik Setiawan</span></td>
                    </tr>
                    <tr>
                      <td>Penerbit</td>
                      <td>:</td>
                      <td><span id="val_penerbit">Start Up</span></td>
                    </tr>
                     <tr>
                      <td>Tahun Terbit</td>
                      <td>:</td>
                      <td><span id="val_thn_terbit">2018</span></td>
                    </tr>
                    <tr>
                      <td>Jumlah</td>
                      <td>:</td>
                      <td><span id="val_jumlah">9</span></td>
                    </tr>
                    <tr>
                      <td>Deskripsi</td>
                      <td>:</td>
                      <td><span id="val_deskripsi">Buku Sakti Pemrograman Web: HTML, CSS, PHP, MySQL & Javascript</span></td>
                    </tr>
                   </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

		