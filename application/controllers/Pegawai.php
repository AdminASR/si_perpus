<?php 

class pegawai extends CI_Controller{

	function __construct(){
		parent::__construct();
    $this->load->model('AdminModel');
	$this->load->model('M_Pegawai');
		 if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
	}
    
  //  function index(){
  //   //Allowing akses to admin only
  //       $data['pegawai'] = $this->M_Pegawai->view();

  //     if($this->session->userdata('jabatan')==='1'){

  //       $this->load->view('header.php',$data);
  //       $this->load->view('pegawai/index',$data);
  //       $this->load->view('footer.php');
  //     }else{
  //         echo "Access Denied";
  //     }

  // }

  //   function petugas(){
  //   //Allowing akses to staff only
  //       $data['pegawai'] = $this->M_Pegawai->view();
  //   if($this->session->userdata('jabatan')==='2'){
  //       $this->load->view('header.php');
  //       $this->load->view('pegawai/index',$data);
  //       $this->load->view('footer.php');
  //   }else{
  //       echo "Access Denied";
  //   }
  // }


  // function author(){
  //   //Allowing akses to author only
  //   if($this->session->userdata('jabatan')==='3'){
  //     $this->load->view('header.php');
  //       $this->load->view('pegawai/index',$data);
  //       $this->load->view('footer.php');
  //   }else{
  //       echo "Access Denied";
  //   }
  // }


    function tambah(){
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
        $this->load->view('pegawai/tambah');
        $this->load->view('admin/footer');
    }
    function tambah_aksi(){
         $user=$this->input->post('user');
          $password=md5($this->input->post('password'));
           $nip=$this->input->post('nip');
            $nama_pegawai=$this->input->post('nama_pegawai');
             $jenis_kelamin=$this->input->post('jenis_kelamin');
              $jabatan=$this->input->post('jabatan');
               $alamat=$this->input->post('alamat');
               $telp=$this->input->post('telp');
       
       
               
        $data= array(
        'user'=>$user,
         'password'=>$password,
         'nip'=>$nip,
         'nama_pegawai'=>$nama_pegawai,
         'jenis_kelamin'=>$jenis_kelamin,
         'jabatan'=>$jabatan,
         'alamat'=>$alamat,
         'telp'=>$telp
         

    
        );
        $this->M_Pegawai->input_data($data,'pegawai');
        redirect('pegawai/index');
    }


    function edit($id_pegawai){
        $where= array ('id_pegawai'=>$id_pegawai);
        $data['pegawai']=$this->M_Pegawai->edit_data($where,'pegawai')->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
        $this->load->view('pegawai/ubah',$data);
        $this->load->view('admin/footer');
    }

    function edit_akun($id_pegawai){
        $where= array ('id_pegawai'=>$id_pegawai);
        $data['pegawai']=$this->M_Pegawai->edit_data($where,'pegawai')->result();
        $data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
        $this->load->view('pegawai/ubah',$data);
        $this->load->view('admin/footer.php');
    }

     


    function update(){
    $id_pegawai = $this->input->post('id_pegawai');
    $user=$this->input->post('user');
          $password=$this->input->post('password');
           $nip=$this->input->post('nip');
            $nama_pegawai=$this->input->post('nama_pegawai');
             $jenis_kelamin=$this->input->post('jenis_kelamin');
              $jabatan=$this->input->post('jabatan');
               $alamat=$this->input->post('alamat');
               $telp=$this->input->post('telp');
       //jika pw kosong, maka pw lama ttap
                if(empty($password)){

                    $data= array(
                    'user'=>$user,
                    'nip'=>$nip,
                    'nama_pegawai'=>$nama_pegawai,
                    'jenis_kelamin'=>$jenis_kelamin,
                    'jabatan'=>$jabatan,
                    'alamat'=>$alamat,
                    'telp'=>$telp);
                     $where = array(
                     'id_pegawai' => $id_pegawai
                    );
                   $this->M_Pegawai->update_data($where,$data,'pegawai');

                     if($this->session->userdata('jabatan')==='1'){
      redirect('home');
    }elseif($this->session->userdata('jabatan')==='2'){
      redirect('home/index_2');
    }else{
        echo "terjadi masalah";
    }


                   
                }else{
                 
                 $data= array(
                'user'=>$user,
                'password'=>md5($password),
                'nip'=>$nip,
                'nama_pegawai'=>$nama_pegawai,
                'jenis_kelamin'=>$jenis_kelamin,
                'jabatan'=>$jabatan,
                'alamat'=>$alamat,
                'telp'=>$telp);
                 $where = array(
                'id_pegawai' => $id_pegawai
                 );

                $this->M_Pegawai->update_data($where,$data,'pegawai');

                if($this->session->userdata('jabatan')==='1'){
       redirect('home');
    }elseif($this->session->userdata('jabatan')==='2'){
     redirect('home/index_2');
    }else{
        echo "terjadi masalah";
    }

                }
    // jika lain, mka pw berubah
       
               
       
    



    
    }


  

    function hapus($id_pegawai){
        $where = array ('id_pegawai'=>$id_pegawai);
        $this->M_Pegawai->hapus_data($where,'pegawai');
        redirect('pegawai/index');
    }
}