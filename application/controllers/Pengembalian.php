<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengembalian extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('AdminModel');
		$this->load->helper('url');
		$this->load->model('M_Pegawai');
		if($this->session->userdata('logged_in') !== TRUE){
			redirect('login');
		}
	}
		
	function index(){
		$data['buku'] = $this->AdminModel->tampil2()->result();
		$data['guru'] = $this->AdminModel->guru()->result();
		$data['siswa'] = $this->AdminModel->tampil1()->result();
		$data['peminjam'] = $this->AdminModel->peminjam()->result();
		$data['gpeminjam'] = $this->AdminModel->gpeminjam()->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/pengembalian',$data);
		$this->load->view('admin/footer');
	}
	//siswa
	function s_detail($id_pinjam){
		$data['detail_s'] = $this->AdminModel->s_detail_2($id_pinjam)->result();
		$data['detail_b'] = $this->AdminModel->s_detail($id_pinjam)->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/s_detail',$data);
		$this->load->view('admin/footer');
	}
	function kembali(){
		$id_pinjam = $this->input->post('id_pinjam');
		$jml_pinjam = $this->input->post('jml');
		$jml_ka = $this->input->post('jml2');
		$kode_buku = $this->input->post('kode_buku');
		$jml_kembali = $this->input->post('jml_kembali');
		$cek_stok=$this->db->query("select jumlah from buku where kode_buku='$kode_buku'")->result();
		$where1=$id_pinjam;
		$where2=$kode_buku;
		$data2=$jml_ka+$jml_kembali;
				$jml1=0;
				foreach ($cek_stok as $row1){
			$jml1=$row1->jumlah;
			if ($data2>$jml_pinjam) {

				redirect('peminjam/s_detail/'.$id_pinjam);
			}
				$upstok=$jml1+$jml_kembali;
				

				$data = array(
					'jumlah' => $upstok
				);
	 
				$where = array(
					'kode_buku' => $kode_buku
				);
				$this->AdminModel->update_data($where,$data,'buku');
				$this->AdminModel->update_data2($where1,$where2,$data2,'detail_peminjaman');
		}
		redirect('pengembalian/s_detail/'.$id_pinjam);
	}
	function lengkap(){
		$id_pinjam = $this->input->post('id_pinjam');
		$status=0;
		$data = array(
					'status' => $status
				);
	 
		$where = array(
					'id_pinjam' => $id_pinjam
				);
				$this->AdminModel->update_data($where,$data,'peminjam');
			redirect('pengembalian');
	}
	function g_detail($id_gpinjam){
		$data['detail_g'] = $this->AdminModel->g_detail_2($id_gpinjam)->result();
		$data['detail_b'] = $this->AdminModel->g_detail($id_gpinjam)->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/g_detail',$data);
		$this->load->view('admin/footer');

	}

	function gkembali(){
	$id_gpinjam = $this->input->post('id_gpinjam');
	$jml_pinjam = $this->input->post('jml');
	$kode_buku = $this->input->post('kode_buku');
	$jml_kembali = $this->input->post('jml_kembali');
	$cek_stok=$this->db->query("select jumlah from buku where kode_buku='$kode_buku'")->result();
			$jml1=0;
			foreach ($cek_stok as $row1){
		$jml1=$row1->jumlah;
		if ($jml_pinjam>$jml_kembali) {
			$upstok=$jml1+$jml_kembali;
			

			$data = array(
				'jumlah' => $upstok
			);
 
			$where = array(
				'kode_buku' => $kode_buku
			);
			$this->AdminModel->update_data($where,$data,'buku');
			$where1=$id_gpinjam;
			$where2=$kode_buku;
			$data2=$jml_kembali;
			$this->AdminModel->update_data3($where1,$where2,$data2,'detail_gpeminjaman');


		}elseif ($jml_pinjam==$jml_kembali) {
						$upstok=$jml1+$jml_kembali;
			

			$data = array(
				'jumlah' => $upstok
			);
 
			$where = array(
				'kode_buku' => $kode_buku
			);
		 
			$this->AdminModel->update_data($where,$data,'buku');
			$where1=$id_gpinjam;
			$where2=$kode_buku;
			$data2=$jml_kembali;
			$this->AdminModel->update_data3($where1,$where2,$data2,'detail_gpeminjaman');

		}
	}

	redirect('pengembalian/g_detail'.$id_gpinjam);
	}
	function glengkap(){
		$id_gpinjam = $this->input->post('id_gpinjam');
		$status=0;
		$data = array(
					'status' => $status
				);
	 
		$where = array(
					'id_gpinjam' => $id_gpinjam
				);
				$this->AdminModel->update_data($where,$data,'gpeminjam');
			redirect('pengembalian');
	}

}
