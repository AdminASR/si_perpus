<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('AdminModel');
		$this->load->helper('url');
		$this->load->model('M_Pegawai');
		if($this->session->userdata('logged_in') !== TRUE){
			redirect('login');
		}

 
	}
 	//<home>
	function index(){
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$data['guru'] = $this->AdminModel->guru()->result();
		$data['siswa'] = $this->AdminModel->tampil1()->result();
		$data['gpengunjung'] = $this->AdminModel->gpengunjung()->result();
		$data['pengunjung'] = $this->AdminModel->pengunjung()->result();
		$data['jumlah1'] = $this->AdminModel->jumlah1()->num_rows();
		$data['jumlah2'] = $this->AdminModel->jumlah2()->num_rows();
		$data['jumlah5'] = $this->AdminModel->jumlah5()->num_rows();
		$data['jumlah3'] = $this->AdminModel->jumlah3();
		$data['jumlah41'] =$this->AdminModel->jumlah41();
		$data['jumlah42'] =$this->AdminModel->jumlah42();
		$data['jumlah43'] =$this->AdminModel->jumlah43();
		$data['jumlah44'] =$this->AdminModel->jumlah44();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/home',$data);
		$this->load->view('admin/footer'); 
	}

	function tambah_pengunjung(){
	$nis = $this->input->post('nis');
	$posisinis1=strpos($nis,"(")+1;
	$posisinis2=strpos($nis,")");
	$ceknis=$posisinis2-$posisinis1;
	$nis=substr($nis,$posisinis1,$ceknis);
	$tanggal = $this->input->post('tanggal');
	$keterangan = $this->input->post('keterangan');
 
		$data = array(
		'nis' => $nis,
		'tanggal' =>$tanggal,
		'keterangan' => $keterangan
			);
		$this->AdminModel->input_data($data,'pengunjung');
		redirect('home/index');
	}

	function tambah_gpengunjung(){
	$no_anggota = $this->input->post('no_anggota');
	$posisinis1=strpos($no_anggota,"(")+1;
	$posisinis2=strpos($no_anggota,")");
	$ceknis=$posisinis2-$posisinis1;
	$no_anggota=substr($no_anggota,$posisinis1,$ceknis);
	$tanggal = $this->input->post('tanggal');
	$keterangan = $this->input->post('keterangan');
 
		$data = array(
		'no_anggota' => $no_anggota,
		'tanggal' =>$tanggal,
		'keterangan' => $keterangan
			);
		$this->AdminModel->input_data($data,'gpengunjung');
		redirect('home/index');
	}
	function edith($id_pengunjung){
		$where = array('id_pengunjung' => $id_pengunjung);
		$data['pengunjung'] = $this->AdminModel->edit_data($where,'pengunjung')->result();
		$this->load->view('admin/index',$data);
	}

	function hapushg($id_gpengunjung){
		$where = array('id_gpengunjung' => $id_gpengunjung);
		$this->AdminModel->hapus_data($where,'gpengunjung');
		redirect('home/index');
	}
	function hapushs($id_pengunjung){
		$where = array('id_pengunjung' => $id_pengunjung);
		$this->AdminModel->hapus_data($where,'pengunjung');
		redirect('home/index');
	}

	function updatehs(){
	$keterangan = $this->input->post('keterangan');
	$id_pengunjung = $this->input->post('id_pengunjung');
 
 
	$data = array(
		'keterangan' => $keterangan
	);
 
	$where = array(
		'id_pengunjung' => $id_pengunjung
	);
 
	$this->AdminModel->update_data($where,$data,'pengunjung');
	redirect('home/index');
	}

	function updatehg(){
	$keterangan = $this->input->post('keterangan');
	$id_gpengunjung = $this->input->post('id_gpengunjung');
 
 
	$data = array(
		'keterangan' => $keterangan
	);
 
	$where = array(
		'id_gpengunjung' => $id_gpengunjung
	);
 
	$this->AdminModel->update_data($where,$data,'gpengunjung');
	redirect('home/index');
	}
	//</home>
}
