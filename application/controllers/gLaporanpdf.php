<?php
Class gLaporanpdf extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->model('AdminModel');
        $this->load->helper('url');
    }
    
    function index(){
        $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','',12);
        // mencetak string 
        $pdf->Image("assets/dist/img/jatim.png",6,10,27);
        $pdf->Image("assets/dist/img/smk.png",180,10,18);
        $pdf->Cell(190,7,'PEMERINTAH PROVINSI JAWA TIMUR',0,1,'C');
        $pdf->Cell(190,7,' DINAS PENDIDIKAN',0,1,'C');
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(190,7,'SEKOLAH MENENGAH KEJURUAN NEGERI 6 JEMBER',0,1,'C');
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(190,7,'Jalan PB. Sudirman 114 Tanggul Telp. / Fax. (0336) 441347 Jember 68155',0,1,'C');
        $pdf->Cell(190,7,'E-mail : smkn6.jember@yahoo.com ; Website : smkn6jember.sch.id',0,1,'C');

        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(37,6,'TANGGAL',1,0);
        $pdf->Cell(60,6,'NAMA GURU',1,0);
        $pdf->Cell(25,6,'JABATAN',1,0);
        $pdf->Cell(65,6,'KETERANGAN',1,1);



        $pdf->SetFont('Arial','',10);
        $where = $this->input->post('tanggal');
        $where2 = $this->input->post('tanggal2');
        $data['tes']=$where;
        $data['tes2']=$where2;
        $laporan = $this->AdminModel->Lgpengunjung($where,$where2,'gpengunjung')->result();
        foreach ($laporan as $row){
            $pdf->Cell(37,6,$row->tanggal,1,0);
            $pdf->Cell(60,6,$row->nama,1,0);
            $pdf->Cell(25,6,$row->jabatan,1,0);
            $pdf->Cell(65,6,$row->keterangan,1,1); 
        }
        $pdf->Cell(10,7,'',0,1);
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','I',8);
        $pdf->Cell(190,3,'Form   : F.05.SAR.6.3 PUS.19',0,1);
        $pdf->Cell(190,3,'Revisi : '.$row->tanggal,0,1);
        $pdf->SetFont('Arial','I',6);
        //$pdf->Cell(190,3,$pdf->Image("assets/dist/img/download.jpg",0 ,0,13),0,1,'R');
        $pdf->Cell(190,3,'Cert. No. 46814/A/0001/UK/En',0,1,'R');

        $pdf->Output();
    }
}