<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Event extends CI_Controller {
  
  public function __construct(){
    parent::__construct();
    
    $this->load->model('M_Event');
  }
  
  public function index(){
    $data['event'] = $this->M_Event->view();
    $this->load->view('buku/view', $data);
  }
  
  public function tambah(){
    $data = array();
    
    if($this->input->post('submit')){ // Jika user menekan tombol Submit (Simpan) pada formhdiashidiman
      // lakukan upload file dengan memanggil function upload yang ada di M_Event.php
      $upload = $this->M_Event->upload();
      
      if($upload['result'] == "success"){ // Jika proses upload sukses
         // Panggil function save yang ada di M_Event.php untuk menyimpan data ke database
        $this->M_Event->save($upload);
        
        redirect('event'); // Redirect kembali ke halaman awal / halaman view data
      }else{ // Jika proses upload gagal
        $data['message'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
      }
    }
    
    $this->load->view('event/form', $data);
  }




  function detail($id){
    $where= array ('id'=>$id);
    $data['event']=$this->M_Event->detail_data_1($where,'event')->result();
    $this->load->view('event/halaman_event',$data);
    
  }


  function detail_event(){
  $judul = $this->input->post('judul');
  $tanggal = $this->input->post('tanggal');
  $deskripsi = $this->input->post('deskripsi');


  $data = array(
    'judul' => $judul,
    'tanggal' => $tanggal,
    'deskripsi' => $deskripsi
  );

  $where = array(
    'id' => $id
  );

  $this->M_Event->detail_data_2($where,$data,'event');
  redirect('event/halaman_event');
  }
  
}