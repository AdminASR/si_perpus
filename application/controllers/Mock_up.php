<?php 
 
 
class Mock_up extends CI_Controller{
 	function __construct(){
		parent::__construct();		

	}
	function home(){
		$this->load->view('mock_user/u_header');
	 	$this->load->view('mock_user/home');
	 	$this->load->view('mock_user/u_footer');
	}
	function katalog(){
	 	$this->load->view('mock_user/u_header');
	 	$this->load->view('mock_user/katalog');
	 	$this->load->view('mock_user/u_footer');
	}
	function kontak(){
	 	$this->load->view('mock_user/u_header');
	 	$this->load->view('mock_user/kontak');
	 	$this->load->view('mock_user/u_footer');
	}
	function event(){
	 	$this->load->view('mock_user/u_header');
	 	$this->load->view('mock_user/event');
	 	$this->load->view('mock_user/u_footer');
	}


}