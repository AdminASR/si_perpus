<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

  function __construct(){
    parent::__construct();    
    $this->load->model('M_Event');
    $this->load->model('AdminModel');
    $this->load->library('upload');
    $this->load->helper('url');
   
 
  }

   function index(){
    $data['event'] = $this->M_Event->view()->result();
    $data['notif2'] = $this->AdminModel->notif()->num_rows();
    $data['notif'] = $this->AdminModel->lpeminjam1()->result();
    $this->load->view('admin/header',$data);
    $this->load->view('admin/event',$data);
    $this->load->view('admin/footer');
  }
  
  function tambah_event(){
      $id   = $this->input->post('id');
      $tanggal    = $this->input->post('tanggal');
      $judul       = $this->input->post('judul');
      $deskripsi    = $this->input->post('deskripsi');
       $status    = $this->input->post('status');
     
      // get foto
      $config['upload_path'] = './images/event/';
      $config['allowed_types'] = 'jpg|png|jpeg|gif';
      $config['max_size'] = '2048';  //2MB max
      $config['max_width'] = '4480'; // pixel
      $config['max_height'] = '4480'; // pixel
      $config['file_name'] = $_FILES['gambar_event']['name'];

      $this->upload->initialize($config);

      if (!empty($_FILES['gambar_event']['name'])) {
          if ( $this->upload->do_upload('gambar_event') ) {
              $gambar = $this->upload->data();
              $data = array(
                            'id'    => $id,
                            'tanggal'     => $tanggal,
                            'judul'        => $judul,
                            'deskripsi'     => $deskripsi,
                            'gambar'         => $gambar['file_name'],
                            'status'    => $status
                          );
       $this->M_Event->tambah($data);
             redirect('event');
          }else {
              die("gagal upload");
          }
      }else {
        echo "tidak masuk";
      }
  }

   function ubah_event(){


  }




  function detail($id){
    $where= array ('id'=>$id);
    $data['event']=$this->M_Event->detail_data_1($where,'event')->result();
    $this->load->view('u_header.php');
    $this->load->view('event/halaman_event',$data);
     $this->load->view('u_footer.php');
    
  }


  function detail_event(){
  $judul = $this->input->post('judul');
  $tanggal = $this->input->post('tanggal');
  $deskripsi = $this->input->post('deskripsi');


  $data = array(
    'judul' => $judul,
    'tanggal' => $tanggal,
    'deskripsi' => $deskripsi
  );

  $where = array(
    'id' => $id
  );

  $this->M_Event->detail_data_2($where,$data,'event');
  redirect('event/halaman_event');
  }


  function hapus($id,$foto){

    $path ='./images/event/';
    @unlink($path.$foto);

    $where = array ('id'=>$id);
    $this->M_Event->hapus($where);
    redirect('event');
  }

  function update_y(){
  $id = $this->input->post('id');
  $status = $this->input->post('status');
  

  $data = array(
    'status' => '1'
  );

  $where = array(
    'id' => $id
  );
  $this->M_Event->update_data($where,$data,'event');
  redirect('event');
  }

  function update_b(){
  $id = $this->input->post('id');
  $status = $this->input->post('status');
  

  $data = array(
    'status' => '0'
  );

  $where = array(
    'id' => $id
  );
  $this->M_Event->update_data($where,$data,'event');
  redirect('event');
  }

}
