<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Buku extends CI_Controller {
  
  public function __construct(){
    parent::__construct();
     $this->load->library('pagination');
    $this->load->model('M_Event');
    $this->load->model('M_Buku');
    $this->load->model('M_Pegawai');
    $this->load->model('M_Komentar');
  }
  
   function index(){
    $data['buku'] = $this->M_Buku->view()->result();
    $data['buku_1'] = $this->M_Buku->view_limit()->result();
    $data['rkmnds_y'] = $this->M_Buku->view_rkmnds_y()->result();
    $data['event'] = $this->M_Event->view_limit()->result();
    $data['event_1'] = $this->M_Event->utama()->result();
    $this->load->view('u_header.php');
    $this->load->view('buku/view', $data);
    $this->load->view('u_footer.php');
  }


  function katalog(){
       $data['bk'] = $this->M_Buku->by_id();
        $this->load->view('u_header.php');
        $this->load->view('buku/katalog',$data);
        $this->load->view('u_footer.php');
        
  }

   function asc(){
       $data['bk'] = $this->M_Buku->by_asc();
        $this->load->view('u_header.php');
        $this->load->view('buku/katalog',$data);
        $this->load->view('u_footer.php');
        
  }
   function desc(){
       $data['bk'] = $this->M_Buku->by_desc();
        $this->load->view('u_header.php');
        $this->load->view('buku/katalog',$data);
        $this->load->view('u_footer.php');
        
  }





  function kontak(){
    $data['pegawai'] = $this->M_Pegawai->view()->result();
    $this->load->view('u_header.php');
    $this->load->view('buku/kontak',$data);
    $this->load->view('u_footer.php');
  }

  function event(){
    $data['event'] = $this->M_Event->view()->result();
    $this->load->view('u_header.php');
    $this->load->view('buku/event',$data);
    $this->load->view('u_footer.php');
  }
  

  public function cari() {
    $this->load->view('u_header.php');
    $this->load->view('buku/katalog');
    $this->load->view('u_footer.php');
  }
 
  public function hasil(){
    $data2['cari'] = $this->M_Buku->cari_buku();
    $this->load->view('u_header.php');
    $this->load->view('buku/result', $data2);
    $this->load->view('u_footer.php');
    
  }

  public function sorting(){
    $data2['cari'] = $this->M_Buku->sort();
    $this->load->view('u_header.php');
    $this->load->view('buku/result', $data2);
    $this->load->view('u_footer.php');
    
  }


    public function hasil_filter(){
    $data2['cari'] = $this->M_Buku->filter_buku();
    $this->load->view('u_header.php');
    $this->load->view('buku/result', $data2);
    $this->load->view('u_footer.php');
    
  }

  function katalog_2(){

   
    $data['buku_1'] = $this->M_Buku->karya_umum()->result();
    $data['buku_2'] = $this->M_Buku->filsafat()->result();
    $data['buku'] = $this->M_Buku->agama()->result();
    $data['buku'] = $this->M_Buku->ilmu_sosial()->result();
    $data['buku'] = $this->M_Buku->bahasa()->result();
    $data['buku'] = $this->M_Buku->ilmu_murni()->result();
    $data['buku'] = $this->M_Buku->ilmu_terapan()->result();
    $data['buku'] = $this->M_Buku->seni_dan_olahraga()->result();
    $data['buku'] = $this->M_Buku->sastra()->result();
    $data['buku'] = $this->M_Buku->sejarah()->result();
    $this->load->view('u_header.php');
    $this->load->view('buku/katalog',$data);
    $this->load->view('u_footer.php');
  }
  // function flsft(){
  //   $data['buku'] = $this->M_Buku->filsafat()->result();
  //   $this->load->view('u_header.php');
  //   $this->load->view('buku/katalog', $data);
  //   $this->load->view('u_footer.php');
  // }
  // function agm(){
  //   $data['buku'] = $this->M_Buku->agama()->result();
  //   $this->load->view('u_header.php');
  //   $this->load->view('buku/katalog', $data);
  //   $this->load->view('u_footer.php');
  // }
  // function ilm_ssl(){
  //   $data['buku'] = $this->M_Buku->ilmu_sosial()->result();
  //   $this->load->view('u_header.php');
  //   $this->load->view('buku/katalog', $data);
  //   $this->load->view('u_footer.php');
  // }
  // function bhs(){
  //   $data['buku'] = $this->M_Buku->bahasa()->result();
  //   $this->load->view('u_header.php');
  //   $this->load->view('buku/katalog', $data);
  //   $this->load->view('u_footer.php');
  // }
  // function ilm_mrn(){
  //   $data['buku'] = $this->M_Buku->ilmu_murni()->result();
  //   $this->load->view('u_header.php');
  //   $this->load->view('buku/katalog', $data);
  //   $this->load->view('u_footer.php');
  // }
  // function ilm_trpn(){
  //   $data['buku'] = $this->M_Buku->ilmu_terapan()->result();
  //   $this->load->view('u_header.php');
  //   $this->load->view('buku/katalog', $data);
  //   $this->load->view('u_footer.php');
  // }
  // function sn_d_olhrg(){
  //   $data['buku'] = $this->M_Buku->seni_dan_olahraga()->result();
  //   $this->load->view('u_header.php');
  //   $this->load->view('buku/katalog', $data);
  //   $this->load->view('u_footer.php');
  // }
  // function sstr(){
  //   $data['buku'] = $this->M_Buku->sastra()->result();
  //   $this->load->view('u_header.php');
  //   $this->load->view('buku/katalog', $data);
  //   $this->load->view('u_footer.php');
  // }
  // function sjrh(){
  //   $data['buku'] = $this->M_Buku->sejarah()->result();
  //   $this->load->view('u_header.php');
  //   $this->load->view('buku/katalog', $data);
  //   $this->load->view('u_footer.php');
  // }
  function tambah_komen(){
      $tanggal=$this->input->post('tanggal'); 
        $komentar=$this->input->post('komentar'); 
        $rating=$this->input->post('rating'); 

        $data= array(
        'tanggal'=>$tanggal,
        'komentar'=>$komentar,
        'rating'=>$rating

    
        );

        if($data == null) {
        $this->session->set_flashdata('msg', 
                '<div class="modal modal-info fade">
                    <h4>Oppss</h4>
                    <p>Tidak ada kata dinput.</p>
                </div>');    
         
    } else {    
        $this->session->set_flashdata('msg', 
        '<ul>
        <span class="close">&times;</span>
  <li><center>Umpan Balik telah Dikirim  </center></li>

</ul>');   
    };

        $this->M_Komentar->input_data($data,'komentar');
        redirect('buku/kontak');
    }


     function petugas(){
    //Allowing akses to staff only
        $data['pegawai'] = $this->M_Pegawai->view();
    if($this->session->userdata('jabatan')==='2'){
        $this->load->view('admin/header.php');
        $this->load->view('pegawai/index',$data);
        $this->load->view('admin/footer.php');
    }else{
        echo "Access Denied";
    }
  }

}