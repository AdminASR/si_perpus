<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekomendasi extends CI_Controller {

	function __construct(){
		parent::__construct();	
		$this->load->model('AdminModel');	
		$this->load->model('M_Buku');
		$this->load->helper('url');
			$this->load->model('M_Pegawai');
		 if($this->session->userdata('logged_in') !== TRUE){
      	redirect('login');
   		 }
	}


	function index(){
		$data['buku'] = $this->M_Buku->view()->result();
		$data['rkmnds_y'] = $this->M_Buku->view_rkmnds_y()->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/rekomendasi',$data);
		$this->load->view('admin/footer');
	}

	function update_y(){
	$kode_buku = $this->input->post('kode_buku');
	$status = $this->input->post('status');
	

	$data = array(
		'status' => '1'
	);

	$where = array(
		'kode_buku' => $kode_buku
	);
	$this->M_Buku->update_data($where,$data,'buku');
	redirect('rekomendasi');
	}


	function update_b(){
	$kode_buku = $this->input->post('kode_buku');
	$status = $this->input->post('status');
	

	$data = array(
		'status' => '0'
	);

	$where = array(
		'kode_buku' => $kode_buku
	);
	$this->M_Buku->update_data($where,$data,'buku');
	redirect('rekomendasi');
	}
}
