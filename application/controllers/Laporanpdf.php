<?php
Class Laporanpdf extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->model('AdminModel');
        $this->load->helper('url');
    }
    
    function index(){
        $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','',12);
        // mencetak string 
        $pdf->Image("assets/dist/img/jatim.png",6,10,27);
        $pdf->Image("assets/dist/img/smk.png",180,10,18);
        $pdf->Cell(190,7,'PEMERINTAH PROVINSI JAWA TIMUR',0,1,'C');
        $pdf->Cell(190,7,' DINAS PENDIDIKAN',0,1,'C');
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(190,7,'SEKOLAH MENENGAH KEJURUAN NEGERI 6 JEMBER',0,1,'C');
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(190,7,'Jalan PB. Sudirman 114 Tanggul Telp. / Fax. (0336) 441347 Jember 68155',0,1,'C');
        $pdf->Cell(190,7,'E-mail : smkn6.jember@yahoo.com ; Website : smkn6jember.sch.id',0,1,'C');

        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(37,6,'TANGGAL',1,0);
        $pdf->Cell(60,6,'NAMA SISWA',1,0);
        $pdf->Cell(25,6,'KELAS',1,0);
        $pdf->Cell(65,6,'KETERANGAN',1,1);



        $pdf->SetFont('Arial','',10);
        $where = $this->input->post('tanggal');
        if ($where==NULL) {
            $tgl=date("Y-m-d");
        }else $tgl=$where;
        $data['tes']=$where;
        $laporan = $this->AdminModel->Lpengunjung($where,'pengunjung')->result();
        foreach ($laporan as $row){
            $pdf->Cell(37,6,$row->tanggal,1,0);
            $pdf->Cell(60,6,$row->nama_siswa,1,0);
            $pdf->Cell(25,6,$row->id_kejur,1,0);
            $pdf->Cell(65,6,$row->keterangan,1,1); 
        }
        $pdf->Cell(10,7,'',0,1);
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','I',8);
        $pdf->Cell(190,3,'Form   : F.05.SAR.6.3 PUS.19',0,1);
        $pdf->Cell(190,3,'Revisi : '.$tgl,0,1);
        //$pdf->Image("assets/dist/img/download.jpg",178,194,13);
        $pdf->SetFont('Arial','I',6);
        $pdf->Cell(190,3,'Cert. No. 46814/A/0001/UK/En',0,1,'R');

        $pdf->Output();
    }
}