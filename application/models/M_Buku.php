<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_Buku extends CI_Model {
  // Fungsi untuk menampilkan semua data buku
 function view_limit(){
       return  $this->db->query("SELECT * FROM `buku` ORDERS LIMIT 9");
  }

  function view(){
       return  $this->db->query("SELECT * FROM `buku`");
  }

  function view_rkmnds_y(){
       return  $this->db->query("SELECT * FROM `buku` where status='1'  ");
  }
  function tambah($data){
      $this->db->insert('buku',$data);
      return TRUE;
  }
 function ubah($data,$where){
      $this->db->update('buku',$data,$where);
      return TRUE;
  }
 function hapus($where) {
      $this->db->where($where);
      $this->db->delete('buku');
      return TRUE;
  }
  function cari_buku(){
    $cari = $this->input->GET('cari', TRUE);
    $data = $this->db->query("SELECT * from buku where judul like '%$cari%' ");
    return $data->result();
  }
  function sort(){
    $urut = $this->input->GET('hehe', TRUE);
    $data = $this->db->query("SELECT * from buku ORDER BY judul $urut");
    return $data->result();
  } 
  function filter_buku(){
    $kd_ktgr = $this->input->GET('voss', TRUE);
    $thn_1 = $this->input->GET('thn1', TRUE);
    $thn_2 = $this->input->GET('thn2', TRUE);
    $data = $this->db->query("SELECT * FROM `buku` WHERE kode_kategori='$kd_ktgr' AND  thn_terbit BETWEEN '$thn_1' AND '$thn_2'");
    return $data->result();
  }
  function update_data($where,$data,$table){
    $this->db->where($where);
    $this->db->update($table,$data);
  }
 
  function by_asc(){
    $this->load->library('pagination'); // Load librari paginationnya
    
    $query = "SELECT * from buku ORDER BY judul ASC"; // Query untuk menampilkan semua data siswa
    
    $config['base_url'] = base_url('buku/asc/');
    $config['total_rows'] = $this->db->query($query)->num_rows();
    $config['per_page'] = 10;
    $config['uri_segment'] = 3;
    $config['num_links'] = 1;
    
    // Style Pagination
    // Agar bisa mengganti stylenya sesuai class2 yg ada di bootstrap
   $config['full_tag_open'] = '<nav><ul class="pagination" style="margin-top:0px">';
$config['full_tag_close'] = '</ul></nav>';
 
$config['first_link'] = 'First';
$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
 
$config['last_link'] = 'Last';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
 
$config['next_link'] = 'Next';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
 
$config['prev_link'] = 'Prev';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
 
$config['cur_tag_open'] = '<li class="active"><a>';
$config['cur_tag_close'] = '</a></li>';
 
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
        // End style pagination
    
    $this->pagination->initialize($config); // Set konfigurasi paginationnya
    
    $page = ($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;
    $query .= " LIMIT ".$page.", ".$config['per_page'];
    
    $data['limit'] = $config['per_page'];
    $data['total_rows'] = $config['total_rows'];
    $data['pagination'] = $this->pagination->create_links(); // Generate link pagination nya sesuai config diatas
    $data['buku'] = $this->db->query($query)->result();
    
    return $data;
    }

    function by_id(){
    $this->load->library('pagination'); // Load librari paginationnya
    
    $query = "SELECT * from buku"; // Query untuk menampilkan semua data siswa
    
    $config['base_url'] = base_url('buku/katalog/');
    $config['total_rows'] = $this->db->query($query)->num_rows();
    $config['per_page'] = 10;
    $config['uri_segment'] = 3;
    $config['num_links'] = 2;
    
    // Style Pagination
    // Agar bisa mengganti stylenya sesuai class2 yg ada di bootstrap
     $config['full_tag_open'] = '<nav><ul class="pagination" style="margin-top:0px">';
$config['full_tag_close'] = '</ul></nav>';
 
$config['first_link'] = 'First';
$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
 
$config['last_link'] = 'Last';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
 
$config['next_link'] = 'Next';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
 
$config['prev_link'] = 'Prev';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
 
$config['cur_tag_open'] = '<li class="active"><a>';
$config['cur_tag_close'] = '</a></li>';
 
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
        // End style pagination
    
    $this->pagination->initialize($config); // Set konfigurasi paginationnya
    
    $page = ($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;
    $query .= " LIMIT ".$page.", ".$config['per_page'];
    
    $data['limit'] = $config['per_page'];
    $data['total_rows'] = $config['total_rows'];
    $data['pagination'] = $this->pagination->create_links(); // Generate link pagination nya sesuai config diatas
    $data['buku'] = $this->db->query($query)->result();
    
    return $data;
    }

    function by_desc(){
    $this->load->library('pagination'); // Load librari paginationnya
    
    $query = "SELECT * from buku ORDER BY judul DESC"; // Query untuk menampilkan semua data siswa
    
    $config['base_url'] = base_url('buku/desc/');
    $config['total_rows'] = $this->db->query($query)->num_rows();
    $config['per_page'] = 10;
    $config['uri_segment'] = 3;
    $config['num_links'] = 2;
    
    // Style Pagination
    // Agar bisa mengganti stylenya sesuai class2 yg ada di bootstrap
       $config['full_tag_open'] = '<nav><ul class="pagination" style="margin-top:0px">';
$config['full_tag_close'] = '</ul></nav>';
 
$config['first_link'] = 'First';
$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
 
$config['last_link'] = 'Last';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
 
$config['next_link'] = 'Next';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
 
$config['prev_link'] = 'Prev';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
 
$config['cur_tag_open'] = '<li class="active"><a>';
$config['cur_tag_close'] = '</a></li>';
 
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
        // End style pagination
    
    $this->pagination->initialize($config); // Set konfigurasi paginationnya
    
    $page = ($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;
    $query .= " LIMIT ".$page.", ".$config['per_page'];
    
    $data['limit'] = $config['per_page'];
    $data['total_rows'] = $config['total_rows'];
    $data['pagination'] = $this->pagination->create_links(); // Generate link pagination nya sesuai config diatas
    $data['buku'] = $this->db->query($query)->result();
    
    return $data;
    }



}